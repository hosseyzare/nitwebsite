<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Base/Base.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Icon/Icon.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Layout/Layout.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Header/Header.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Footer/Footer.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Form/Form.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/text-box-tab.css">

  <script src="<?=baseUrl()?>/js/jquery-1.11.3.min.js"></script>

</head>
<style>
  #content {
    padding: 50px;
  }
  #content .center-cetner-container {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  #content .tab-box-favorit{
    transition: 0.3s;
    width: 100%;
    height:100px;
    background-color: white;
    box-shadow: 0 0 5px 1px rgba(1, 99, 165, 0.1);
    border-radius: 5px;
    padding:5px;
    box-sizing: border-box;
    direction:ltr;
    cursor: text;
  }
  #content .tab-box-favorit:hover{
    width: 100%;
    height:100px;
    background-color: white;
    box-shadow: 0 0 5px 1px rgba(1, 99, 165, 0.2);
    border-radius: 5px;

  }
  #content .tab-box-favorit.active{
    box-shadow: 0 0 5px 1px rgba(1, 99, 165, 0.4);
  }
</style>
<body>
<?=$content?>
</body>
</html>
