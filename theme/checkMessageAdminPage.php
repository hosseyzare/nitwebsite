<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Base/Base.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Dashboard/Dashboard.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Admin/Admin.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Form/Form.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/text-box-tab.css">

  <script src="<?=baseUrl()?>/js/jquery-1.11.3.min.js"></script>
  <style>
    #content {
      padding: 50px;
    }
    #content .center-cetner-container {
      display: flex;
      flex-direction: column;
      align-items: center;
    }
    #content .tab-box-favorit{
      transition: 0.3s;
      width: 100%;
      height:max-content;
      padding:5px;
      box-sizing: border-box;
      direction:ltr;
    }
    #content textarea[disabled]{
      min-height: max-content;
      max-height: max-content;
      resize: none;
    }
    #content input[disabled],
    #content textarea[disabled],
    #content input[disabled]:hover,
    #content textarea[disabled]:hover{
      background-color: transparent;
      color:black;
      box-shadow: none;
    }
  </style>
</head>
<body>
<?=$content?>
</body>
</html>
