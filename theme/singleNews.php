<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Base/Base.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Icon/Icon.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Layout/Layout.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Header/Header.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Footer/Footer.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Grid/Grid.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Modal.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/news.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/Comment-Tree/Comment-Tree.css">

  <script src="<?=baseUrl()?>/js/jquery-1.11.3.min.js"></script>

</head>
<style>
  .container {
    padding: 50px;
  }

  .container .title {
    padding: 5px 10px;
  }

  .container .item-news {
    box-shadow: 0 0 20px 1px rgba(70, 131, 180, 0.4);
  }

  .container .title-comment-box {
    font-size: 25px;
    font-weight: bold;
    margin: 0;
  }

  .container .comment-text-box {
    padding: 3px 20px;
  }

  .container .comment-text-box .comment-text {
    background-color: transparent;
    height:51px;
  }

  .comment-box-container {
    padding: 20px 30px;
  }
  .container .comments-container{
    border-top: 2px dashed rgb(150, 107, 0);
    margin-top:50px;
    padding:20px 0;
  }
  .container .comments-container .title-comment-box{
    padding:0 10px;
  }
</style>
<body>
<?=$content?>
<?=$footer?>
</body>
</html>
