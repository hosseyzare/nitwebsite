<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Base/Base.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Dashboard/Dashboard.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Admin/Admin.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Form/Form.css">

  <script src="<?=baseUrl()?>/js/jquery-1.11.3.min.js"></script>
  <style>
    #root{
      position: fixed;
      width: 100%;
      height:100%;
      top:0;
      left:0;
      padding:0;
      display: flex;
      align-items: center;
      justify-content: center;
    }
  </style>
</head>
<body>
<?=$content?>
</body>
</html>
