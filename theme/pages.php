<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Base/Base.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Icon/Icon.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Layout/Layout.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Header/Header.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Footer/Footer.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Form/Form.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/text-box-tab.css">
  <link rel="stylesheet" href="<?=baseUrl()?><?=$css?>">


  <script src="<?=baseUrl()?>/js/jquery-1.11.3.min.js"></script>

</head>
<body>
<?=$content?>
<?=$footer?>
</body>
</html>
