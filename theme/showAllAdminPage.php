<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Grid/Grid.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Base/Base.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Dashboard/Dashboard.css">
  <link rel="stylesheet" href="<?=baseUrl()?>/css/Admin/Admin.css">

  <script src="<?=baseUrl()?>/js/jquery-1.11.3.min.js"></script>
</head>
<body>
<div id="header-wrapper">
  <div id="header"></div>
</div>
<div id="content"><?=$content?></div>
</body>
</html>
