class Comment {
    constructor(commentId, replyCommentId, sender, message, comments, current = null) {
        this.commentId = commentId;
        this.replyCommentId = replyCommentId;
        this.sender = sender;
        this.message = message;
        this.comments = comments;
        this.current = current;
        this.currentInput = null;
        this.actionType = {
            REPLY: "REPLY",
            GO_TO_PROFILE: "SENDER",
            SEE_MORE: "SEE_MORE",
        };
        this.reply = null;
        this.senderNode = null;
        this.seeMore = null;
        this.isColaps = false;
        this.forCurrentInput = "comment-" + this.commentId;
    }
    setHandler = (handler, node, action) => $(node).on("click", (e) => handler(this, action));

    setReplyHandler = (handler) => this.setHandler(handler, this.reply, this.actionType.REPLY);

    setGoToPtofileHandler = (handler) => this.setHandler(handler, this.senderNode, this.actionType.GO_TO_PROFILE);

    setSeeMoreHandler = (handler) => this.setHandler(handler, this.seeMore, this.actionType.SEE_MORE);

    checkedCurrentInput = () => $(this.currentInput).attr({ checked: true });
    unCheckedCurrentInput = () => $(this.currentInput).attr({ checked: false });
    setSpinnerSeeMore = () => $(this.seeMore).addClass("spinner").addClass("isLoad").removeAttr("for");
    unSetSpinnerSeeMore = () => $(this.seeMore).removeClass("spinner").removeClass("isLoad").attr({ for: this.forCurrentInput });
    Load() {
        this.checkedCurrentInput();
        this.setSpinnerSeeMore();
    }
    unLoad() {
        this.unSetSpinnerSeeMore();
        // this.unCheckedCurrentInput();
    }
    render(rerender = false) {
        if (!rerender && this.current) {

            return;
        }
        const comment = $("<li>").addClass("comment").attr({ "comment-id": this.commentId });
        const commentDitaleBox = $("<div>").addClass("comment-ditale-box");
        this.currentInput = $("<input>")
            .addClass("see-more-checkbox")
            .attr({ type: "checkbox", id: this.forCurrentInput });

        const textBox = $("<div>").addClass("text-box");

        this.senderNode = $("<span>").addClass("sender-comment");
        const senrderName = $("<span>").addClass("sender-name").html(this.sender.username);
        const senrderPic = $("<div>").addClass("sender-pic");
        $(this.senderNode).append(senrderPic).append(senrderName);
        if (!this.sender.pic)
            $(senrderPic).addClass("no-pic");
        else {
            const img = $("<img>").attr({ src: this.sender.pic });
            $(senrderPic).append(img);
        }
        const message = $("<p>").addClass("message-comment").html(this.message);
        $(textBox).append(this.senderNode).append(message);

        const endBoxContainer = $("<div>").addClass("end-box-container");
        const replyBox = $("<div>").addClass("reply-box");
        this.reply = $("<span>").addClass("reply-comment").html("پاسخ دادن");
        $(replyBox).append(this.reply);
        $(endBoxContainer).append(replyBox);

        $(commentDitaleBox).append(textBox).append(endBoxContainer);
        $(comment).append(this.currentInput).append(commentDitaleBox);

        this.current = comment;
    }
    addSeeMore() {
        const seeMoreBox = $("<div>").addClass("see-more-box");
        this.seeMore = $("<label>")
            .addClass("see-more-comment")
            .attr({ for: this.forCurrentInput })

        $(seeMoreBox).append(this.seeMore);

        $(this.current)
            .find(".end-box-container")
            .append(seeMoreBox);

    }
    removeInnerCurrent(){
        this.removeSeeMore();
        $(this.current).find(" > .tree-comments").remove();
    }
    removeSeeMore(){
        $(this.seeMore).parent().remove();
    }
    addToCurrent(node) {
        $(this.current).append(node);
    }
}