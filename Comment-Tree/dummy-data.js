let additionCommentId = 2000;
const dataGetFetched = {
    commentId: additionCommentId,
    replyCommentId: null,
    sender: {
        id: 1,
        username: "سجاد سیفی",
        pic: "",
    },
    message: "",
};
const dataSendedToServer = {
    userToken: "",
    message: "",
    replyCommentId: null,
};
const commentsDummy = [
    {
        commentId: 1,
        replyCommentId: null,
        sender: {
            id: 1,
            username: "سجاد سیفی",
            pic: "",
        },
        message: "سلام من خوبم تو چطوری لولو",
        comments: [
            {
                sender: {
                    id: 1,
                    username: "حسین زارع",
                    pic: "",
                },
                replyCommentId: 1,
                commentId: 2,
                message: "سلام من خوبم تو چطوری لولو",
                comments: [

                ]
            },
            {
                sender: {
                    id: 1,
                    username: "سجاد سیفی",
                    pic: "",
                },
                replyCommentId: 1,
                commentId: 4,
                message: "سلام من خوبم تو چطوری لولو",
                comments: [

                ]
            },
            {
                sender: {
                    id: 5,
                    username: "رضا مقدم",
                    pic: "",
                },
                replyCommentId: 1,
                commentId: 5,
                message: "سلام من خوبم تو چطوری لولو",
                comments: [
                    {
                        sender: {
                            id: 5,
                            username: "مملی",
                            pic: "",
                        },
                        replyCommentId: 5,
                        commentId: 6,
                        message: "سلام من خوبم تو  لولو",
                        comments: [

                        ]
                    }
                ]
            }
        ]
    },
    {
        sender: {
            id: 2,
            username: "سلام علیکم علیکم سلام",
            pic: "",
        },
        replyCommentId: null,
        commentId: 7,
        message: "سلام من خوبم تو چطوری لولو",
        comments: [
            {
                sender: {
                    id: 2,
                    username: "88989",
                    pic: "",
                },
                replyCommentId: 7,
                commentId: 10,
                message: "بالای بالا انگار رو ابرا",
                comments: [

                ]
            }
        ]
    }
];