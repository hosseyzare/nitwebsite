//api => /add
//api => /more
class CommentTree {
    constructor(container, comments, isFake = false) {
        this.comments = comments;
        this.container = container;
        this.handler = this.handlerCommentTree;
        this.tree = null;
        this.messageBox = null;
        this.senderBtn = null;
        this.currentReplyComment = null;
        this.containerBase = null;
        this.ignordClass = ["reply-comment", "message-text-box-to-call-server", "btn-to-call-server"];
        this.API = "";
        this.isFake = isFake;
        this.isCallServerAddComment = false;
        this.intervalTimerObj = null;
        this.topInterVal = 0;
        this.TREE = null;
    }
    setHandler(handler) {
        this.handler = handler;
    }
    addServerHandler(api, messageBox, senderBtn) {
        this.messageBox = messageBox;
        this.senderBtn = senderBtn;
        this.API = api;
        $(this.senderBtn).addClass("btn-to-call-server");
        $(this.messageBox).addClass("message-text-box-to-call-server");
        $(document).on("click", (e) => {

            for (let i = 0; i < this.ignordClass.length; i++)
                if ($(e.target).hasClass(this.ignordClass[i]))
                    return;

            this.unSelectForReply();

        });
        $(this.senderBtn).on("click", (e) => {
            if (this.isCallServerAddComment)
                return;
            const commentText = $(messageBox).val();
            dataSendedToServer.message = commentText;
            dataGetFetched.commentId++;
            dataGetFetched.message = commentText;
            if (this.currentReplyComment) {
                dataSendedToServer.replyCommentId = this.currentReplyComment.commentId;
                dataGetFetched.replyCommentId = this.currentReplyComment.commentId;
            }
            else {
                dataSendedToServer.replyCommentId = null;
                dataGetFetched.replyCommentId = null;
            }
            this.callServerToAddComment(dataSendedToServer);
        });
    }
    getAllCommentsFromApi(isfake) {
        $(this.containerBase).append("درحال بارگذاری...");
        $.ajax({
            url: isfake ? "" : this.API,
            data: {},
            type: "GET",
            contentType: "application/json",
            success: (res) => {
                //for test
                this.comments = commentsDummy;
                this.render();
            },
            error: (e) => {
                // alert(e.responseText + "");
                $(this.containerBase).append("داده ای موجود نیست");

            }
        })
    }
    loadingAddComment() {
        this.isCallServerAddComment = true;
        $(this.senderBtn)
            .addClass("spinner")
            .addClass("isLoad");
    }
    finishedLoadinAddComment() {
        this.isCallServerAddComment = false;
        $(this.senderBtn)
            .removeClass("spinner")
            .removeClass("isLoad");
    }
    callServerToAddComment(data) {
        if (!data.message || data.message.length == 0)
            return;
        this.loadingAddComment();
        $.ajax({
            url: this.isFake ? "" : this.API,//api
            data: JSON.stringify(data),
            type: "GET",
            contentType: "application/json",
            success: (res) => {
                this.addSingComment(this.currentReplyComment, dataGetFetched);
                this.clearMessageBox();
                this.unSelectForReply();
                this.finishedLoadinAddComment();
            },
            error: (e) => {
                alert(e.responseText + "  پیام شما ثبت نشده است");
                this.unSelectForReply();
                this.finishedLoadinAddComment();
            }
        })
    }
    get getScrollTop() {
        const posEl = $(this.messageBox).position();
        const winSize = $(window).height();

        const top = posEl.top - (winSize / 2);
        return Math.floor(top);
    }
    clearMessageBox = () => $(this.messageBox).val("");
    selectForReply(comment) {
        $(comment.current).addClass("select-for-replyed");
        this.currentReplyComment = comment;
    }
    unSelectForReply() {
        this.currentReplyComment = null;
        $(".select-for-replyed").removeClass("select-for-replyed");
    }
    handlerCommentTree = (comment, action) => {
        switch (action) {
            case comment.actionType.REPLY: this.replyCommentRun(comment);
                break;
            case comment.actionType.GO_TO_PROFILE: console.log("GO_TO_PROFILE");
                break;
            case comment.actionType.SEE_MORE: this.seeMoreGetDataFromServer(comment);
                break;
            default: console.log("action is not handled");
        }
    }
    moveToTop() {
        if (this.topInterVal == null || window.scrollY <= this.topInterVal) {
            this.topInterVal = null;
            return;
        }
        window.scrollTo(0, window.scrollY - 20);

        setTimeout(() => this.moveToTop(), 5);
    }
    Tree(treeArr, parent) {

        if (treeArr == undefined || treeArr == null || treeArr.length == 0) {
            return null;
        }
        const treeNode = $("<ul>").addClass("tree-comments");
        // const tree = new Tree();
        const subTree = treeArr.map(
            (commentJson) => {
                const comment = this.getComment(commentJson);
                const innerCurrent = this.Tree(comment.comments);
                if (innerCurrent != null) {
                    this.addSeeMoreRelpyComment(comment, innerCurrent);
                }
                // tree.push(innerCurrent);
                return comment;
            }
        );

        // parent.push(tree);

        const subTreeNode = subTree.map(leaf => leaf.current);
        return $(treeNode).append(subTreeNode);
    }
    pushSubTree(treeNode, treeArr) {
        const subTree = this.Tree(treeArr);
        return $(treeNode).append(subTree);
    }
    popSubTree(treeNode) {
        return $(treeNode).parent().remove(treeNode);
    }
    addSingComment(replyedComment, json) {
        if (replyedComment) {
            if (replyedComment.seeMore == null) {
                let subTree = this.Tree([json]);
                this.addSeeMoreRelpyComment(replyedComment, subTree);
                $(replyedComment.current).find(".tree-comments").prepend(tree);
            }
            else {

                let comment = this.getComment(json);
                $(replyedComment.current).find("> .tree-comments").prepend(comment.current);
            }
            this.openComment(replyedComment.current);
        } else {
            let comment = this.getComment(json);
            $(this.tree).prepend(comment.current);
        }
    }
    addSeeMoreRelpyComment(comment, innerCurrent) {
        comment.addSeeMore();
        comment.addToCurrent(innerCurrent);
        comment.setSeeMoreHandler(this.handler);
    }
    getComment(commentJson) {
        const { commentId, replyCommentId, sender, message, comments } = commentJson;
        const comment = new Comment(commentId, replyCommentId, sender, message, comments);
        comment.render();
        comment.setGoToPtofileHandler(this.handler);
        comment.setReplyHandler(this.handler);

        return comment;
    }
    openTree(tree) {
        $(tree).find(" > .comment > input").attr({ checked: true });
    }
    openComment(comment) {
        $(comment).find(" > input").attr({ checked: true });
    }
    render() {
        if (this.containerBase == null) {
            this.containerBase = $("<div>").addClass("container-base-tree-comments");
            $(this.container).append(this.containerBase);
        }

        this.tree = this.Tree(this.comments) || $("<ul>").addClass("tree-comments");
        $(this.containerBase).empty().append(this.tree);
        this.openTree(this.tree);


    }
    replyCommentRun(comment) {
        $(this.messageBox).focus();
        this.unSelectForReply();
        this.selectForReply(comment);
        this.topInterVal = this.getScrollTop;
        this.moveToTop(this.topInterVal);
    }
    seeMoreGetDataFromServer(comment) {
        console.log("SEE_MORE");

        setTimeout(() => {
            const inputClicked = $(comment.currentInput).get()[0];
            console.log(inputClicked, inputClicked.checked);
            if (inputClicked.checked)
                this.getReplyedCommentsById(comment);
        }, 100);
    }
    getReplyedCommentsById(comment) {
        comment.Load();
        
        $.ajax({
            url:this.isFake ? "" +this.API  : this.API,
            data: JSON.stringify({ id: comment.commentId }),
            contentType: "application/json",
            type: "GET",
            success: (res) => {
                comment.unLoad();
                const data = res.d;

                if (false && (data == null || data == undefined || data.length == 0) ){
                    comment.removeInnerCurrent();
                    return;
                }

                console.log(commentsDummy);
                comment.removeInnerCurrent();
                const subTree = this.Tree(commentsDummy);
                this.addSeeMoreRelpyComment(comment, subTree);
                this.openTree(subTree);
            },
            error: (e) => {
                comment.unLoad();
                console.log("error")
            }
        });
    }
}