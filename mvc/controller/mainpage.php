<?php


class MainPageController {
  private $serverStatus = array(
    'server1' => 'working',
    'server2' => 'notworking',
    'server4' => 'working',
    'server5' => 'working'
  );
  public function __construct(){

  }
  public function loadpage(){

    $data['newsRecord'] = MainPageModel::getLatestNews(8);
    $data['servicesRecord'] = MainPageModel::getServices();
    View::render('/mainpage/home.php',$data);
  }
  public function ajaxServerReq(){
    echo json_encode($this->serverStatus);
  }

}