<?php
class PageController {
  public function aboutus(){
    $data['css'] ='/css/pages/karkonan.css';
    View::renderPages('/pages/aboutus.php',$data);
  }
  public function aboutCorp(){
    $data['css'] ='/css/pages/meet-nit.css';
    View::renderPages('/pages/aboutcorp.php',$data);
  }
  public function contactus(){
    $data['css'] ='/css/pages/contact-us.css';
    View::renderPages('/pages/contactus.php',$data);

  }
  public function aboutmanager(){
    $data['css'] ='/css/pages/manager.css';
    View::renderPages('/pages/manager.php',$data);
  }
  public function notfound(){
    echo '404';
    exit;
  }
}