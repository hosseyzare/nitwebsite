<?php

class UserController {

  public function __construct(){
  }

  public function logout(){
    session_destroy();
    header("Location: " . fullBaseUrl() . "/user/login");
  }

  public function profile($p1, $p2, $p3){
    echo "Profile Executed";
    echo "$p1, $p2, $p3";
  }

  public function login() {
    if (isset($_POST['username'])){
      $this->loginCheck();
    } else {
      $this->loginForm();
    }
  }

  private function loginCheck(){


    $response = array();
    $response['dashboard'] = baseUrl() . '/dashboard/addnews' ;
    $email = $_POST['username'];
    $password = $_POST['password'];
    $response['test']=$email;
    $record = UserModel::fetch_by_email($email);
    if ($record == null) {
      $response['state'] = false;
    } else {
     // $hashedPassword = encryptPassword($password);
      if ($password == $record['password']) {
        $_SESSION['email'] = $record['email'];
        $_SESSION['user_id'] = $record['user_id'];
        $_SESSION['fullname'] = $record['fullname'];

        $response['state'] = true;
      } else {
        $response['state'] = false;
      }
    }

    echo json_encode($response);

  }

  private function loginForm(){
    $data['test'] = array();
    View::renderLoginPage("/user/login.php", $data);
  }



}