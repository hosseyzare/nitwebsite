<?php


class BlogController {
  public function __construct(){

  }

  public function page($pageIndex){

    $contentCountPerPage = 5;
    $startIndex = ($pageIndex-1) * $contentCountPerPage;
    $data['results'] = BlogModel::catalogByPage($startIndex,$contentCountPerPage);
    $recordsCount = BlogModel::countNews();
    $data['pageCount'] = ceil($recordsCount/$contentCountPerPage);
    $data['pageIndex'] = $pageIndex;

    
    View::render("/blog/blog.php",$data);

  }
  public function news($newsId){
    $newsRecord = BlogModel::loadOneNews($newsId);
    $commentRecords = BlogModel::loadComments($newsId);
    $data['newsRecord'] = $newsRecord;
    $data['commentRecords'] = $commentRecords;
    View::renderSingleNews("/blog/single.php",$data);
    }

}