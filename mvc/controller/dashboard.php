<?php


class DashboardController {
  private $IsAdmin = false;

  public function __construct(){
//    if(isset($_SESSION['email'])){
//      $this->IsAdmin = true;
//    }else{
//      $data['test'] = array();
//      View::renderLoginPage("/user/login.php", $data);
//      exit;
//    }
  }
  public function home(){
    echo "This Is Home";
  }
  public function addNews(){

    view::renderaddAdminPage('/dashboard/addnews.php');
  }
  public function ajaxAddNews(){
    $title = '';
    $description = '';
    $author = '';
    $text = '';
    $time = getCurrentDateTime();;


    if ( 0 < $_FILES['file']['error'] ) {
      echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
      move_uploaded_file($_FILES['file']['tmp_name'], 'image/thumbnail/' . $_FILES['file']['name']);
    }
    $thumbnail= $_FILES['file']['name'];
    BlogModel::insertNews($title, $description, $author, $text, $time, $thumbnail);

  }
  public function EditNews($id){
    $data['newsId'] = $id;
    view::renderaddAdminPage('/dashboard/editnews.php',$data);
  }
  public function ajaxEditNews($id){
    $title = '';
    $description = '';
    $author = '';
    $text = '';
    $time = getCurrentDateTime();;


    if ( 0 < $_FILES['file']['error'] ) {
      echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
      move_uploaded_file($_FILES['file']['tmp_name'], 'image/thumbnail/' . $_FILES['file']['name']);
    }
    $thumbnail= $_FILES['file']['name'];
    BlogModel::updateNews($id,$title, $description, $author, $text, $time, $thumbnail);

  }
  public function ShowAllNews(){
    view::rendershowAllAdminPage('/dashboard/showallnews.php');
  }
  public function AjaxRemoveNews($id){
    //$id = $_POST["id"];
    BlogModel::deletitemById($id);
  }
  public function allNewsAjaxSender(){
    $indexPage = (int)$_POST["indexPage"]; // last = -1 , first = 0
    $itemCount = (int)$_POST["itemCount"];
    $allCountRow = BlogModel::countRow();

    $maxPage = ceil($allCountRow / $itemCount);

    if ($indexPage < 0) {
      $indexPage = $maxPage;
    } else if ($indexPage > $maxPage) {
      $indexPage = 1;
    }
    $limit = ($indexPage - 1) * $itemCount;
    $limit = abs($limit);

    $records = BlogModel::catalogByPage($limit, $itemCount);

    $data['maxPage'] = $maxPage;
    $data['indexPage'] = $indexPage;
    $data['gridItems'] = array();

    $data['itemCount'] = $itemCount;
    if (is_array($records)) {
      $imax = count($records);
    } else if (is_null($records)) {
      $imax = 0;
    } else {
      $imax = 1;
    }
    for ($i = 0; $i < $imax; $i++) {
      $data['gridItems'][$i]['id'] = $records[$i]['news_id'];
      $data['gridItems'][$i]['pic'] = fullImageUrl($records[$i]['thumbnail']);
      $data['gridItems'][$i]['options']['a_title'] = $records[$i]['title'];
      $data['gridItems'][$i]['options']['b_author'] = $records[$i]['author'];
     $data['gridItems'][$i]['options']['c_NewsTime'] = date('j/ n/ Y', strtotime($records[$i]['newsTime']));

    }
    echo json_encode($data);
  }

  //services
  public function addServices(){

    View::renderaddAdminPage('/dashboard/addService.php');

  }
  public function ajaxAddServices() {
    $title = '';
    $description = '';
    $link = '';


    if ( 0 < $_FILES['file']['error'] ) {
      echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
      move_uploaded_file($_FILES['file']['tmp_name'], 'image/thumbnail/' . $_FILES['file']['name']);
    }
    $thumbnail= $_FILES['file']['name'];
    ServiceModel::insertService($title,$description,$link,$thumbnail);
  }


  public function showAllServices(){
    View::rendershowAllAdminPage('/dashboard/showallservices.php');

  }
  public function AjaxRemoveService($id){
    //$id = $_POST["id"];
    ServiceModel::removeServices($id);
  }
  public function allServiceAjaxSender(){
    $indexPage = (int)$_POST["indexPage"]; // last = -1 , first = 0
    $itemCount = (int)$_POST["itemCount"];
    $allCountRow = ServiceModel::countRow();

    $maxPage = ceil($allCountRow / $itemCount);

    if ($indexPage < 0) {
      $indexPage = $maxPage;
    } else if ($indexPage > $maxPage) {
      $indexPage = 1;
    }
    $limit = ($indexPage - 1) * $itemCount;
    $limit = abs($limit);

    $records = ServiceModel::catalogByPage($limit, $itemCount);

    $data['maxPage'] = $maxPage;
    $data['indexPage'] = $indexPage;
    $data['gridItems'] = array();

    $data['itemCount'] = $itemCount;
    if (is_array($records)) {
      $imax = count($records);
    } else if (is_null($records)) {
      $imax = 0;
    } else {
      $imax = 1;
    }
    for ($i = 0; $i < $imax; $i++) {
      $data['gridItems'][$i]['id'] = $records[$i]['service_id'];
      $data['gridItems'][$i]['pic'] = fullImageUrl($records[$i]['thumbnail']);
      $data['gridItems'][$i]['options']['a_title'] = $records[$i]['title'];
      $data['gridItems'][$i]['options']['b_author'] = $records[$i]['link'];

    }
    echo json_encode($data);
  }
  public function editService(){
    View::renderaddAdminPage('/dashboard/editService.php');

  }
  public function ajaxEditServices($id){
    $title = '';
    $description = '';
    $link = '';



    if ( 0 < $_FILES['file']['error'] ) {
      echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
      move_uploaded_file($_FILES['file']['tmp_name'], 'image/thumbnail/' . $_FILES['file']['name']);
    }
    $thumbnail= $_FILES['file']['name'];
    ServiceModel::updateService($id,$title,$description,$link,$thumbnail);

  }

  //helps
  public function addHelps(){
    View::renderaddAdminPage('/dashboard/addHelps.php');

  }
  public function ajaxAddHelps(){
    $title = '';
    $description = '';
    $link = '';
    $category = '';
    $serviceId = '';


    if ( 0 < $_FILES['file']['error'] ) {
      echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
      move_uploaded_file($_FILES['file']['tmp_name'], 'image/thumbnail/' . $_FILES['file']['name']);
    }
    $thumbnail= $_FILES['file']['name'];
    HelpModel::insertHelp($title,$description,$link,$category,$serviceId);
  }
  public function showAllHelps(){
    View::rendershowAllAdminPage('/dashboard/showallHelps.php');

  }
  public function editHelp(){
    View::renderaddAdminPage('/dashboard/editHelps.php');

  }

  public function ajaxEditHelps($id){
    $title = '';
    $description = '';
    $link = '';



    if ( 0 < $_FILES['file']['error'] ) {
      echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
      move_uploaded_file($_FILES['file']['tmp_name'], 'image/thumbnail/' . $_FILES['file']['name']);
    }
    $thumbnail= $_FILES['file']['name'];
    HelpModel::updateHelp($id,$title,$description,$link,$thumbnail);

  }
  public function AjaxRemoveHelp($id){
    //$id = $_POST["id"];
    HelpModel::removeHelps($id);
  }
  public function allHelpAjaxSender(){
    $indexPage = (int)$_POST["indexPage"]; // last = -1 , first = 0
    $itemCount = (int)$_POST["itemCount"];
    $allCountRow = HelpModel::countRow();

    $maxPage = ceil($allCountRow / $itemCount);

    if ($indexPage < 0) {
      $indexPage = $maxPage;
    } else if ($indexPage > $maxPage) {
      $indexPage = 1;
    }
    $limit = ($indexPage - 1) * $itemCount;
    $limit = abs($limit);

    $records = HelpModel::catalogByPage($limit, $itemCount);

    $data['maxPage'] = $maxPage;
    $data['indexPage'] = $indexPage;
    $data['gridItems'] = array();

    $data['itemCount'] = $itemCount;
    if (is_array($records)) {
      $imax = count($records);
    } else if (is_null($records)) {
      $imax = 0;
    } else {
      $imax = 1;
    }


    for ($i = 0; $i < $imax; $i++) {
      $serviceId = $records[$i]['service_id'];
      $servicesData = ServiceModel::loadOneService($serviceId);
      $data['gridItems'][$i]['id'] = $records[$i]['help_id'];
      $data['gridItems'][$i]['options']['a_title'] = $records[$i]['title'];
      $data['gridItems'][$i]['options']['b_category'] = $records[$i]['category'];
      $data['gridItems'][$i]['options']['b_service'] = $servicesData['title'];
   //   $data['gridItems'][$i]['options']['b_link'] = $records[$i]['link'];

    }
    echo json_encode($data);
  }

  //messages
  public function ShowAllCareerMessage(){
    View::rendershowAllAdminPage('/dashboard/showallcareerMessage.php');
    $result = DashboardModel::loadCareer();
    //dump($result);

  }
  public function AjaxRemoveCareer($id){
      //$id = $_POST["id"];
      CareerModel::deletitemById($id);
  } 
  //messages
  public function AjaxSenderAllCareerDashboard(){

      $indexPage = (int)$_POST["indexPage"]; // last = -1 , first = 0
      $itemCount = (int)$_POST["itemCount"];
      $allCountRow = CareerModel::countRowCareer();

      $maxPage = ceil($allCountRow / $itemCount);

      if ($indexPage < 0) {
        $indexPage = $maxPage;
      } else if ($indexPage > $maxPage) {
        $indexPage = 1;
      }
      $limit = ($indexPage - 1) * $itemCount;
      $limit = abs($limit);

      $records = CareerModel::catalogByPage($limit, $itemCount);

      $data['maxPage'] = $maxPage;
      $data['indexPage'] = $indexPage;
      $data['gridItems'] = array();

      $data['itemCount'] = $itemCount;
      if (is_array($records)) {
        $imax = count($records);
      } else if (is_null($records)) {
        $imax = 0;
      } else {
        $imax = 1;
      }
      for ($i = 0; $i < $imax; $i++) {
        $data['gridItems'][$i]['id'] = $records[$i]['career_id'];
        $data['gridItems'][$i]['options']['a_firstname'] = $records[$i]['firstname'];
        $data['gridItems'][$i]['options']['b_lastname'] = $records[$i]['lastname'];
        $data['gridItems'][$i]['options']['c_email'] = $records[$i]['email'];
        $data['gridItems'][$i]['options']['d_phonenumber'] = $records[$i]['phonenumber'];
        $data['gridItems'][$i]['options']['e_condition'] = $records[$i]['status'];
      }
      echo json_encode($data);

    
  }



  public function checkcareerMessage($id){
    $data['careerMessage'] = $id;
    View::renderCheckMessageAdminPage('/dashboard/checkCareerMessage.php',$data);

  }
  public function AjaxCheckData(){

    $id = $_POST['id'];
    $record = CareerModel::SearchCareerMessage($id);

    $data = array();
    $data['firstname'] = $record[0]['firstname'];
    $data['lastname'] = $record[0]['lastname'];
    $data['email'] = $record[0]['email'];
    $data['phone']= $record[0]['phonenumber'];
    $data['favarits']= $record[0]['interest'];
    $data['message']= $record[0]['text'];
    $data['condition'] = $record[0]['status'];

    echo json_encode($data);


  }

  public function AjaxStatusCareer($message){
    $id = $_POST['id'];
    if($message == 'cancle'){
      $data = 0;
      CareerModel::UpdateStatus($id,$data);

    }elseif ($message == 'accept'){
      $data = 1;
      echo 'i am here';
      CareerModel::UpdateStatus($id,$data);
    }else{
      echo 'error';
    }
  }
  public function ShowAllContactToUsMessage(){
    View::rendershowAllAdminPage('/dashboard/showallcontactMessage.php');

  }
  public function AjaxRemoveContactToUs($id){
    //$id = $_POST["id"];
    ContactModel::deletitemById($id);
  }
  //messages
  public function AjaxSenderAllContactToUsDashboard(){

    $indexPage = (int)$_POST["indexPage"]; // last = -1 , first = 0
    $itemCount = (int)$_POST["itemCount"];
    $allCountRow = ContactModel::countRow();

    $maxPage = ceil($allCountRow / $itemCount);

    if ($indexPage < 0) {
      $indexPage = $maxPage;
    } else if ($indexPage > $maxPage) {
      $indexPage = 1;
    }
    $limit = ($indexPage - 1) * $itemCount;
    $limit = abs($limit);

    $records = ContactModel::catalogByPage($limit, $itemCount);

    $data['maxPage'] = $maxPage;
    $data['indexPage'] = $indexPage;
    $data['gridItems'] = array();

    $data['itemCount'] = $itemCount;
    if (is_array($records)) {
      $imax = count($records);
    } else if (is_null($records)) {
      $imax = 0;
    } else {
      $imax = 1;
    }
    for ($i = 0; $i < $imax; $i++) {
      $data['gridItems'][$i]['id'] = $records[$i]['contact_id'];
      $data['gridItems'][$i]['options']['a_fullname'] = $records[$i]['fullname'];
      $data['gridItems'][$i]['options']['c_email'] = $records[$i]['email'];
      $data['gridItems'][$i]['options']['d_title'] = $records[$i]['title'];
    }
    echo json_encode($data);


  }


  public function ShowAllComments(){
    View::rendershowAllAdminPage('/dashboard/showallcommentMessage.php');

  }

  public function AjaxRemoveComment($id){
    //$id = $_POST["id"];
    CommentModel::deletitemById($id);
  }
  //messages
  public function AjaxSenderAllCommentDashboard(){

    $indexPage = (int)$_POST["indexPage"]; // last = -1 , first = 0
    $itemCount = (int)$_POST["itemCount"];
    $allCountRow = CommentModel::countRow();

    $maxPage = ceil($allCountRow / $itemCount);

    if ($indexPage < 0) {
      $indexPage = $maxPage;
    } else if ($indexPage > $maxPage) {
      $indexPage = 1;
    }
    $limit = ($indexPage - 1) * $itemCount;
    $limit = abs($limit);

    $records = CommentModel::catalogByPage($limit, $itemCount);

    $data['maxPage'] = $maxPage;
    $data['indexPage'] = $indexPage;
    $data['gridItems'] = array();

    $data['itemCount'] = $itemCount;
    if (is_array($records)) {
      $imax = count($records);
    } else if (is_null($records)) {
      $imax = 0;
    } else {
      $imax = 1;
    }
    for ($i = 0; $i < $imax; $i++) {
      $status = $records[$i]['status'];
      if($status != null){
        if($status==0){
          $StatusString = 'رد شده';
        }elseif ($status==1){
          $StatusString = 'تایید شده';
        }
      }else{
        $StatusString = 'نا مشخص';
      }

      $data['gridItems'][$i]['id'] = $records[$i]['comment_id'];
      $data['gridItems'][$i]['options']['a_fullname'] = $records[$i]['username'];
      $data['gridItems'][$i]['options']['b_email'] = $records[$i]['email'];
      $data['gridItems'][$i]['options']['c_NewsTime'] = date('j/ n/ Y', strtotime($records[$i]['date']));
      $data['gridItems'][$i]['options']['d_status'] = $StatusString;
    }
    echo json_encode($data);


  }
  public function AjaxStatusComment($message){
    $id = $_POST['id'];
    if($message == 'cancle'){
      $data = 0;
      CommentModel::UpdateStatus($id,$data);

    }elseif ($message == 'accept'){
      $data = 1;
      CommentModel::UpdateStatus($id,$data);
    }else{
      echo 'error';
    }
    echo json_encode(true);
  }
  public function checkCommentMessage($id) {
    $data['CommentMessage'] = $id;
    View::renderCheckMessageAdminPage('/dashboard/checkCommentMessage.php', $data);

  }
  public function AjaxCheckComment(){

    $id = $_POST['id'];
    $record = CommentModel::SearchCommentMessage($id);

    $data = array();
    $data['fullname'] = $record[0]['username'];
    $data['email'] = $record[0]['email'];
    $data['date']= date('j/ n/ Y', strtotime($record[0]['date']));
    $data['text']= $record[0]['text'];


    $status = $record[0]['status'];
    if($status != null){
      if($status==0){
        $StatusString = 'رد شده';
      }elseif ($status==1){
        $StatusString = 'تایید شده';
      }
    }else{
      $StatusString = 'نا مشخص';
    }
    $data['status'] = $StatusString;
    echo json_encode($data);


  }

  // 404 error
  public function stillworking(){
    echo ' هنوز تکمیل نشده است ...';
  }

}