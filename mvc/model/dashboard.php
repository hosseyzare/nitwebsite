<?php


class DashboardModel {
  public static function loadCareer(){
    $db = Db::getInstance();
    $records = $db->query("SELECT * FROM nit_careers");
    return $records;
  }
  public static function countRow($table){
    $db = Db::getInstance();
    $records = $db->query("SELECT COUNT(*) AS total FROM $table");
    return $records[0]['total'];
  }

}