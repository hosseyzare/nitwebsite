<?php


class ServiceModel {
  public static function loadAllServices(){
    $db = Db::getInstance();
    $records = $db->query("SELECT * FROM nit_services");
    return $records;
  }
  public static function countServices(){
    $db = Db::getInstance();
    $records = $db->query("SELECT COUNT(*) AS total FROM nit_services");
    return $records[0]['total'];
  }
  public static function loadOneService($id){
    $db = Db::getInstance();
    $records = $db->first("SELECT * FROM nit_services WHERE service_id = $id");
    return $records;
  }
  public static function insertService($title,$description,$link,$thumbnail) {
    $db = Db::getInstance();
    $db->insert("INSERT INTO nit_services
 
      (  title,  detail,  link,    thumbnail) VALUES
      ('$title', '$description',  '$link','$thumbnail')"
    );
  }
  public static function updateService($id,$title,$description,$link,$thumbnail) {
    $db = Db::getInstance();
    $db->modify("UPDATE nit_services SET title=$title ,description = $description,thumbnail= $thumbnail WHERE service_id=$id");

  }
  public static function removeServices($service_id){
    $db = Db::getInstance();
    $db->modify("DELETE FROM  nit_services WHERE service_id=$service_id");
  }
  public static function countRow(){
    return DashboardModel::countRow("nit_services");
  }
  public static function catalogByPage($startIndex,$count){
    $db = Db::getInstance();
    $records = $db->query("SELECT * FROM nit_services LIMIT $startIndex,$count");
    return $records;
  }


}