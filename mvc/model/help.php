<?php


class HelpModel {
  public static function loadAllHelps(){
    $db = Db::getInstance();
    $records = $db->query("SELECT * FROM nit_helps");
    return $records;
  }
  public static function countServices(){
    $db = Db::getInstance();
    $records = $db->query("SELECT COUNT(*) AS total FROM nit_helps");
    return $records[0]['total'];
  }
  public static function loadOneHelp($id){
    $db = Db::getInstance();
    $records = $db->first("SELECT * FROM nit_helps WHERE help_id = $id");
    return $records;
  }
  public static function insertHelp($title,$description,$link,$category,$serviceId) {
    $db = Db::getInstance();
    $db->insert("INSERT INTO nit_helps
 
      (  title,  text,  category,    service_id,link,link) VALUES
      ('$title', '$description',  '$link','$category','$link')"
    );
  }
  public static function updateHelp($id,$title,$description,$link,$thumbnail) {
    $db = Db::getInstance();
    $db->modify("UPDATE nit_helps SET title=$title ,description = $description,thumbnail= $thumbnail WHERE help_id=$id");

  }
  public static function removeHelps($service_id){
    $db = Db::getInstance();
    $db->modify("DELETE FROM  nit_helps WHERE help_id=$service_id");
  }
  public static function countRow(){
    return DashboardModel::countRow("nit_helps");
  }
  public static function catalogByPage($startIndex,$count){
    $db = Db::getInstance();
    $records = $db->query("SELECT * FROM nit_helps LIMIT $startIndex,$count");
    return $records;
  }

}