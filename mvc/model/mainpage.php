<?php


class MainPageModel {
  public static function getLatestNews($count) {
    $db = Db::getInstance();
    $records = $db->query("SELECT * FROM nit_news ORDER BY newsTime DESC LIMIT $count");
    return $records;
  }
  public static function getServices() {
    $db = Db::getInstance();
    $records = $db->query("SELECT * FROM nit_services");
    return $records;
  }
}