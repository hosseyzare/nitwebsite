<div class="root-admin">
  <side class="side">
    <div class="sid-container-box">
      <div class="profile-box">
        <div class="img-user-holder">
          <img src="<?=baseUrl()?>/image/icons/adminlogo.png" />
        </div>
        <div class="full-name">پنل ادمین</div>
      </div>
      <div class="dashboard Dashboard-Menu-CDD">
        <!-- <ul class="container-dashborad-item">
        <li class="item-dashborad active">
            <h4 class="title-item">خبر</h4>
            <ul class="container-sub-itms">
                <li class="sub-itms active-sub">افزودن</li>
                <li class="sub-itms">ویرایش</li>
                <li class="sub-itms">نمایش همه</li>
            </ul>
        </li>
        <li class="item-dashborad ">
            <h4 class="title-item">اساتید</h4>
            <ul class="container-sub-itms">
                <li class="sub-itms active-sub">افزودن</li>
                <li class="sub-itms">ویرایش</li>
                <li class="sub-itms">نمایش همه</li>
            </ul>
        </li>
    </ul> -->
      </div>
    </div>
  </side>
  <section id='content'>
    <div class="center-center-container">
      <div class="center-center-box">
        <h1 class="title-form">ویرایش راهنما</h1>
        <form class="form" onsubmit="return false">
          <div class="form-group">
            <label for="title">عنوان راهنما</label>
            <input type="text" name="title" id="title" />
          </div>
          <div class="form-group">
            <label for="linkVideo">لینک فیلم آموزشی</label>
            <input type="text" name="linkVideo" id="linkVideo" />
          </div>
          <div class="form-group">
            <label for="describtion">توضیحات راهنمای جدید</label>
            <textarea name="describtion" id="describtion"></textarea>
          </div>
          <div class="form-group-2">
            <label>راهنما برای نوع کاربر</label>
            <div class="select-group">
              <input type="radio" typeCall="TEACHER" name="rd-add-gude" id="teacher-rd" />
              <label for="teacher-rd">اساتید</label>

              <input type="radio" typeCall="STUDENT" name="rd-add-gude" id="student-rd" />
              <label for="student-rd">دانشجو</label>

              <input type="radio" typeCall="KARKONAN" name="rd-add-gude" id="karkonan-rd" />
              <label for="karkonan-rd">کارکنان</label>

              <input type="radio" typeCall="PUBLIC" name="rd-add-gude" id="pulbic-rd" />
              <label for="pulbic-rd">عمومی</label>
            </div>
          </div>
          <!-- <div class="form-group-2">
            <label>قابل نمایش برای</label>
            <div class="select-group">
              <input type="radio" typeCall="SHOW" name="rd-add-news" id="show-rd" />
              <label for="show-rd">نمایش</label>

              <input type="radio" typeCall="NOT_SHOW" name="rd-add-news" id="not-show-rd" />
              <label for="not-show-rd">غیر قابل نمایش</label>
            </div>
          </div> -->
          <div class="form-group center-center-container">
                            <span class="button-form btn-send-form success" onclick="addGude()">
                                <span class="spinner "></span>
                                <span class="btn-title">
                                    ویرایش
                                </span>
                            </span>
            <span class="button-form btn-send-form dangres" onclick="removeGude()">
                                <span class="spinner "></span>
                                <span class="btn-title">
                                    حذف
                                </span>
                            </span>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
<script src="<?=baseUrl()?>/js/dashboard-dami.js"></script>
<script src="<?=baseUrl()?>/js/Form.js"></script>
<script>
  const title = document.querySelector("form #title");
  const describtion = document.querySelector("form #describtion");
  const API_EDIT_NEWS = "http://localhost:5000/news/1";
  const API_DELETE_NEWS = "http://localhost:5000/news/delete/1";
  function addGude() {

    if ($(".btn-send-form").hasClass("clicked"))
      return;

    const selectedShoable = document.querySelector("input[name=rd-add-show]:checked");
    const selected = document.querySelector("input[name=rd-add-gude]:checked");
    if (title.value == "" || describtion.value == "" || selected == null || selectedShoable == null) {
      alert("لطفا تمامی فیلد ها را پر کنید");
      $(".btn-send-form").removeClass("clicked").find(".isLoad").toggleClass("isLoad");
      return;
    }
    const typeUser = "" || selected.getAttribute("typeCall");
    const isShow = selected.getAttribute("typeCall") == "SHOW";
    const data = {
      title,
      describtion,
      typeUser,
      isShow
    };
    callServer({
      url: API_EDIT_NEWS,
      data,
      type: "POST",
    }, 0);
  }
  function removeGude() {
    callServer({
      url: API_DELETE_NEWS,
      type: "GET",
      error: (e) => {
        console.log("DELTE");
      }
    }, 1);
  }
</script>