<div class="root-admin">
  <side class="side">
    <div class="profile-box">
      <div class="img-user-holder">
        <img src="<?=baseUrl()?>/image/icons/adminlogo.png" />
      </div>
      <div class="ditale-user">
        <span class="full-name">سجاد سیفی لر</span>
        <span class="type-user">ادمین کل</span>
      </div>
    </div>
    <div class="dashboard Dashboard-Menu-CDD">
    </div>
  </side>
  <section id="content">
    <div class="container">

    </div>

  </section>
</div>
<script src="<?=baseUrl()?>/js/dashboard-dami.js"></script>
<script src="<?=baseUrl()?>/js/Grid.js"></script>
<script>
  dataDummy = {
    gridItems: [
      { id: 1, options: { title: "عنوان", showable: "بله", userType: "استاد", link: "https://", } }
      , { id: 2, options: { title: "عنوان", showable: "بله", userType: "استاد", link: "https://", } }
      , { id: 3, options: { title: "عنوان", showable: "بله", userType: "استاد", link: "https://", } }
      , { id: 4, options: { title: "عنوان", showable: "بله", userType: "استاد", link: "https://", } }
      , { id: 5, options: { title: "عنوان", showable: "بله", userType: "استاد", link: "https://", } }
    ],
    ShowNumberitems: 5,
    indexPage: 1,
    maxPage: 20,
  };
  const gridContainer = document.querySelector('#content .container');
  const title = "مشاهده راهنما ها";
  const optionsViewItemNumber = [
    { value: "5", title: "5" },
    { value: "10", title: "10" },
    { value: "15", title: "15" },
  ];
  const optionsViewItemWidthSomthing = [
    { value: "TITLE", title: "عنوان" },
    { value: "LAST_EDIT", title: "اخرین ویرایش" },
    { value: "START_ADD", title: "تاریخ ثبت" },
    { value: "ُSHOWABLE", title: "قابل نمایش" },
    { value: "ُNOT_SHOWABLE", title: "غیر قابل نمایش" },
  ];
  const headerTitles = [
    "عنوان",
    "دسته بندی آموزش",
    "سرویس مربوطه",
    "مدیریت"
  ];
  const API = "http://localhost:8080/nit/dashboard/allHelpAjaxSender";
  const DELETE_API = "http://localhost:8080/nit/dashboard/AjaxRemoveHelp";
  CreateGrid({
    gridContainer: gridContainer,
    gridTitle: title,
    optionsViewItemNumber: optionsViewItemNumber,
    optionsViewItemWidthSomthing: optionsViewItemWidthSomthing,
    headerTitles: headerTitles,
    API: API,
    deleteAPI:DELETE_API,
    pageEdit:"/dashboard/checkcareerMessage",
  });
</script>