<div class="root-admin">
  <side class="side">
    <div class="sid-container-box">
      <div class="profile-box">
        <div class="img-user-holder">
          <img src="<?=baseUrl()?>/image/icons/adminlogo.png" />
        </div>
        <div class="full-name">پنل ادمین</div>
      </div>
      <div class="dashboard Dashboard-Menu-CDD">
      </div>
    </div>
  </side>
  <section id='content'>
    <div class="center-center-container">
      <div class="center-center-box">
        <h1 class="title-form">اضافه کردن سرویس</h1>
        <form class="form" onsubmit="return false">
          <div class="img-hodler-selected">
            <img src="" />
          </div>
          <div class="form-group">
            <label for="title">عنوان سرویس</label>
            <input type="text" name="title" id="title" />
          </div>
          <div class="form-group">
            <label for="link-service">لینک سامانه سرویس</label>
            <input type="text" name="link-service" id="link-service" />
          </div>
          <div class="form-group">
            <label for="describtion">توضیحات سرویس</label>
            <textarea type="text" class="mini" name="describtion" id="describtion" value=""></textarea>
          </div>
          <!-- <div class="select-group">
            <input type="radio" typeCall="SHOW" name="rd-add-news" id="show-rd" />
            <label for="show-rd">نمایش</label>

            <input type="radio" typeCall="NOT_SHOW" name="rd-add-news" id="not-show-rd" />
            <label for="not-show-rd">غیر قابل نمایش</label>
          </div> -->
          <div class="form-group">
            <label for="file-upload-pic"> ایکون سرویس ها </label>
            <input type="file" class="file-upload-pic" name="file-upload-pic" id="file-upload-pic">
            <label class="file-item" for="file-upload-pic">
              <div class="file-box">
              </div>
            </label>
          </div>
          <div class="form-group center-center-container">
                            <span class="button-form btn-send-form" onclick="addService()">
                                <span class="spinner "></span>
                                <span class="btn-title">
                                    افزودن
                                </span>
                            </span>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
<script src="<?=baseUrl()?>/js/dashboard-dami.js"></script>
<script src="<?=baseUrl()?>/js/Form.js"></script>
<script>
  const title = document.querySelector("form #title");
  const linkService = document.querySelector("form #link-service");
  const describtion = document.querySelector("form #link-service");
  const fileIconUlpad = null;
  const API_ADD_SERVICE = "http://localhost:5000/";
  function addService() {
    if ($(".btn-send-form").hasClass("clicked"))
      return;

    const selectedShoable = document.querySelector("input[name=rd-add-show]:checked");
    if (title.value == "" || describtion.value == "" || linkService.value == "" || selectedShoable==null) {
      alert("لطفا تمامی فیلد ها را پر کنید");
      return;
    }
    const isShow = selected.getAttribute("typeCall") == "SHOW";
    const data = {
      title,
      linkService,
      describtion,
      isShow
    };
    callServer({
      url: API_ADD_SERVICE,
      data,
      type: "POST",
    }, 0);
  }
</script>