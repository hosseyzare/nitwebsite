

<div class="root-admin">
  <side class="side">
    <div class="sid-container-box">
      <div class="profile-box">
        <div class="img-user-holder">
          <img src="<?=baseUrl()?>/image/icons/adminlogo.png" />
        </div>
        <div class="full-name">پنل ادمین</div>
      </div>
      <div class="dashboard Dashboard-Menu-CDD">
      </div>
    </div>
  </side>
  <section id='content'>
    <div class="center-center-container">
      <div class="center-center-box">
        <h1 class="title-form">فرم درخواست همکاری</h1>
        <form class="form" onsubmit="return false">
          <div class="form-row">
            <div class="form-group">
              <label for="firstname">نام</label>
              <input type="text" name="firstname" id="firstname" value="سجاد" disabled />
            </div>
            <div class="form-group">
              <label for="lastname">نام خانوادگی</label>
              <input type="text" name="lastname" id="lastname" value="سیفی" disabled />
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="email">رایان نامه الکتریکی</label>
              <input type="email" name="email" id="email" value="s.09334497255@gmail.com" disabled  />
            </div>
            <div class="form-group">
              <label for="phone">شماره تماس</label>
              <input type="text" name="phone" id="phone" value="09334497255" disabled />
            </div>
            <div class="form-group">
              <label for="status">وضعیت</label>
              <input type="text" name="status" id="status" value="1" disabled />
            </div>
          </div>
          <div class="form-group">
            <label for="favarits">زمینه های مورد علاقه</label>
            <div class="tab-box-favorit" name="favarits" disabled>

            </div>
            <!-- <textarea name="favarits" id="favarits" class="mini"></textarea> -->
          </div>
          <div class="form-group">
            <label for="message">متن پیام</label>
            <textarea name="message" id="message" disabled>سلام من سجاد سیفی هستم و درخواست همکاری دارم</textarea>
          </div>
          <div class="form-group center-center-container" style="margin-top:20px;">
                            <span class="button-form success btn-send-form" type="ACCSEPT" >
                                <span class="spinner "></span>
                                <span class="btn-title">
                                    تایید درخواست
                                </span>
                            </span>
            <span class="button-form rebeccapurple btn-send-form" type="CANCEL" >
                                <span class="spinner "></span>
                                <span class="btn-title">
                                    رد درخواست
                                </span>
                            </span>
            <span class="button-form dangres btn-send-form" type="DELETE" >
                                <span class="spinner "></span>
                                <span class="btn-title">
                                    حذف درخواست
                                </span>
                            </span>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
<script src="<?=baseUrl()?>/js/dashboard-dami.js"></script>
<script src="<?=baseUrl()?>/js/Form.js"></script>
<script>
  let idCooperation= <?=$careerMessage?>;
  const API_SINGLEDATA = "http://localhost:8080/nit/dashboard/AjaxCheckData";
  const API_DELETE = "http://localhost:8080/nit/dashboard/AjaxRemoveCareer";
  const API_CANCEL = "http://localhost:8080/nit/dashboard/AjaxStatusCareer/cancle";
  const API_ACCSEPT = "http://localhost:8080/nit/dashboard/AjaxStatusCareer/accept";
  const ApiAndIndex = {
    "ACCSEPT":{index:0,api:API_ACCSEPT},
    "CANCEL":{index:1,api:API_CANCEL},
    "DELETE":{index:2,api:API_DELETE},
    "SINGLEDATA":{index:3,api:API_SINGLEDATA}
  };
  let {api,index} = ApiAndIndex["SINGLEDATA"];
  sendRequest(api,index);


  function setFormData(formData){
    console.log(formData);
    $("input[name=firstname]").val(formData.firstname);
    $("input[name=lastname]").val(formData.lastname);
    $("input[name=email]").val(formData.email);
    $("input[name=phone]").val(formData.phone);
    $("input[name=status]").val(formData.status?'تایید شده':'تایید نشده');
    $("textarea[name=message]").val(formData.message);
    const favaritNode = formData.favarits.split(',').map(f=>{
      const title =  $("<sapn>").addClass("text-tab-entity").html(f);

      return $("<sapn>").addClass("tab-entity").append(title);
    });
    $("[name=favarits]").addClass("text-box-tab").append(favaritNode);
  }
  $(".btn-send-form").on("click",function(e){
    const type= $(this).attr("type");
    const {api,ind} = ApiAndIndex[type];
    sendRequest(api,ind);
  });
  
  function sendRequest(API,index) {
    console.log('iss :' + API,index);
    callServer({
      url: API,
      data:{id:idCooperation},
      type: "POST",
      success:(formData) =>setFormData(formData)

    },index);
  }

</script>