<div class="root-admin">
  <side class="side">
    <div class="profile-box">
      <div class="img-user-holder">
        <img src="<?=baseUrl()?>/image/icons/adminlogo.png" />
      </div>
      <div class="ditale-user">
        <span class="full-name">سجاد سیفی لر</span>
        <span class="type-user">ادمین کل</span>
      </div>
    </div>
    <div class="dashboard Dashboard-Menu-CDD">
    </div>
  </side>
  <section id="content">
    <div class="container">

    </div>

  </section>
</div>
<script src="<?=baseUrl()?>/js/dashboard-dami.js"></script>
<script src="<?=baseUrl()?>/js/Grid.js"></script>
<script>
  dataDummy = {
    gridItems: [
      { id: 1, options: { fullname: "سجاد سیفی", email: "s.09334497255@gmail.com", phone: "+98 933 449 7255", condition: "تایید شده", } },
      { id: 2, options: { fullname: "حسین زارع", email: "hossey@gmail.com", phone: "+98 911 774 4412", condition: "رد شده", } },
      { id: 3, options: { fullname: "امین رستمی", email: "aminrostami@gmail.com", phone: "+98 935 385 1385", condition: "تایید شده", } },
      { id: 4, options: { fullname: "داداش داداش", email: "dadashdadash@gmail.com", phone: "+98 999 999 9999", condition: "حذف شده", } },
      { id: 5, options: { fullname: "بی نام", email: "binam@gmail.com", phone: "+98 900 000 0000", condition: "رد شده", } },
    ],
    ShowNumberitems: 5,
    indexPage: 1,
    maxPage: 20,
  };
  const gridContainer = document.querySelector('#content .container');
  const title = "مشاهده تماس با ما";
  const optionsViewItemNumber = [
    { value: "5", title: "5" },
    { value: "10", title: "10" },
    { value: "15", title: "15" },
  ];
  const optionsViewItemWidthSomthing = [
    { value: "َALL", title: "همه" },
    { value: "ACCSEPT", title: "تایید شده" },
    { value: "CANCEL", title: "رد شده" },
    { value: "ُDELETE", title: "حذف شده" },
  ];
  const headerTitles = [
    "نام ونام خانوادگی",
    "ایمیل",
    "شماره تماس",
    "وضعیت",
    "مدیریت"
  ];
  const API = "http://localhost:8080/nit/dashboard/AjaxSenderAllContactToUsDashboard";
  const DELETE_API = "http://localhost:8080/nit/dashboard/AjaxRemoveContactToUs";
  CreateGrid({
    gridContainer: gridContainer,
    gridTitle: title,
    optionsViewItemNumber: optionsViewItemNumber,
    optionsViewItemWidthSomthing: optionsViewItemWidthSomthing,
    headerTitles: headerTitles,
    API: API,
    deleteAPI:DELETE_API,
    pageEdit:"/dashboard/checkcareerMessage",
  });
</script>