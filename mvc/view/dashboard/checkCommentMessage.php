

<div class="root-admin">
  <side class="side">
    <div class="sid-container-box">
      <div class="profile-box">
        <div class="img-user-holder">
          <img src="<?=baseUrl()?>/image/icons/adminlogo.png" />
        </div>
        <div class="full-name">پنل ادمین</div>
      </div>
      <div class="dashboard Dashboard-Menu-CDD">
      </div>
    </div>
  </side>
  <section id='content'>
    <div class="center-center-container">
      <div class="center-center-box">
        <h1 class="title-form">فرم تماس با ما</h1>
        <form class="form" onsubmit="return false">
          <div class="form-row">
            <div class="form-group">
              <label for="fullname">نام و نام خانوادگی</label>
              <input type="text" name="fullname" id="fullname" value="سجاد" disabled />
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <label for="email">ایمیل</label>
              <input type="email" name="email" id="email" value="s.09334497255@gmail.com" disabled  />
            </div>
            <div class="form-group">
              <label for="date">تاریخ</label>
              <input type="text" name="date" id="date" value="09334497255" disabled />
            </div>
            <div class="form-group">
              <label for="status">وضعیت</label>
              <input type="text" name="status" id="status" value="1" disabled />
            </div>
          </div>
          <div class="form-group">
            <label for="message">متن پیام</label>
            <textarea name="text" id="text" disabled>سلام من سجاد سیفی هستم و درخواست همکاری دارم</textarea>
          </div>
          <div class="form-group center-center-container" style="margin-top:20px;">
                            <span class="button-form success btn-send-form" type="ACCSEPT" >
                                <span class="spinner "></span>
                                <span class="btn-title">
                                    تایید کامنت
                                </span>
                            </span>
            <span class="button-form rebeccapurple btn-send-form" type="CANCEL" >
                                <span class="spinner "></span>
                                <span class="btn-title">
                                    رد کامنت
                                </span>
                            </span>
            <span class="button-form dangres btn-send-form" type="DELETE" >
                                <span class="spinner "></span>
                                <span class="btn-title">
                                    حذف کامنت
                                </span>
                            </span>
          </div>
        </form>
      </div>
    </div>
  </section>
</div>
<script src="<?=baseUrl()?>/js/dashboard-dami.js"></script>
<script src="<?=baseUrl()?>/js/Form.js"></script>
<script>
  let idCooperation= <?=$CommentMessage?>;
  const API_SINGLEDATA = "http://localhost:8080/nit/dashboard/AjaxCheckComment";
  const API_DELETE = "http://localhost:8080/nit/dashboard/AjaxRemoveComment";
  const API_CANCEL = "http://localhost:8080/nit/dashboard/AjaxStatusComment/cancle";
  const API_ACCSEPT = "http://localhost:8080/nit/dashboard/AjaxStatusComment/accept";
  const ApiAndIndex = {
    "ACCSEPT":{index:0,api:API_ACCSEPT},
    "CANCEL":{index:1,api:API_CANCEL},
    "DELETE":{index:2,api:API_DELETE},
    "SINGLEDATA":{index:3,api:API_SINGLEDATA}
  };
  let {api,index} = ApiAndIndex["SINGLEDATA"];
  sendRequest(api,index);


  function setFormData(formData){
    console.log(formData);
    $("input[name=fullname]").val(formData.fullname);
    $("input[name=email]").val(formData.email);
    $("input[name=date]").val(formData.date);
    $("input[name=status]").val(formData.status);
    $("textarea[name=text]").val(formData.text);
    console.log(formData.status);
  //   const favaritNode = formData.favarits.split(',').map(f=>{
  //     const title =  $("<sapn>").addClass("text-tab-entity").html(f);
  //
  //     return $("<sapn>").addClass("tab-entity").append(title);
  //   });
  //   $("[name=favarits]").addClass("text-box-tab").append(favaritNode);
   }
  $(".btn-send-form").on("click",function(e){
    const type= $(this).attr("type");
    const {api,ind} = ApiAndIndex[type];
    sendRequestStatus(api,ind);
  });
  function sendRequest(API,index) {
    console.log('iss :' + API,index);
    callServer({
      url: API,
      data:{id:idCooperation},
      type: "POST",
      success:(formData) =>setFormData(formData)

    },index);
  }
  function sendRequestStatus(API,index) {
    console.log('iss :' + API,index);
    callServer({
      url: API,
      data:{id:idCooperation},
      type: "POST"

    },index);
  }

</script>