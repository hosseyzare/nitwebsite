<div class="root-admin">
  <side class="side">
    <div class="profile-box">
      <div class="img-user-holder">
        <img src="<?=baseUrl()?>/image/icons/adminlogo.png" />
      </div>
      <div class="ditale-user">
        <span class="full-name">سجاد سیفی لر</span>
        <span class="type-user">ادمین کل</span>
      </div>
    </div>
    <div class="dashboard Dashboard-Menu-CDD">
    </div>
  </side>
  <section id="content">
    <div class="container">

    </div>

  </section>
</div>
<script src="<?=baseUrl()?>/js/dashboard-dami.js"></script>
<script src="<?=baseUrl()?>/js/Grid.js"></script>
<script>
  const gridContainer = document.querySelector('#content .container');
  const title = "مشاهده خبرها";
  const optionsViewItemNumber = [
    { value: "5", title: "5" },
    { value: "10", title: "10" },
    { value: "15", title: "15" },
  ];
  const optionsViewItemWidthSomthing = [
    { value: "TITLE", title: "عنوان" },
    { value: "LAST_EDIT", title: "اخرین ویرایش" },
    { value: "START_ADD", title: "تاریخ ثبت" },
    { value: "END_DATE", title: "تاریخ اتمام" },
  ];
  const headerTitles = [
    "تصویر",
    "عنوان خبر",
    "نویسنده",
    "تاریخ ثبت",
    "مدیریت"
  ];
  const API = "http://localhost:8080/nit/dashboard/allNewsAjaxSender";
  const DELETE_API = "http://localhost:8080/nit/dashboard/AjaxRemoveNews";
  CreateGrid({
    gridContainer: gridContainer,
    gridTitle: title,
    optionsViewItemNumber: optionsViewItemNumber,
    optionsViewItemWidthSomthing: optionsViewItemWidthSomthing,
    headerTitles: headerTitles,
    API: API,
    deleteAPI:DELETE_API,
    pageEdit:"/dashboard/checkcareerMessage",
  });

</script>