
<section id='content'>
  <header class="top-section-moarefi">
    <div class="box-container-diatle">
      <div>
      <h1 class="title-page">مرکز فناوری اطلاعات</h1>
      <span class="sp-noshirvani">نوشیروانی</span>
      <p class="describtion">
        این مرکز در سال 1372 کار خود را اغاز کرد و هدف آن ارائه خدمات رایانه ای به دانشجویان اعضای هیت علمی
        و کارمندان است. هدف اصلی مرکز راه اندازی زیر ساخت سریع فیبر نوری وتجهیز مرکز سورو تخصصی است. </p>

      <form class="form-rel-uni" action="<?=baseUrl()?>/career/requestform" method="post">
        <h3 class="title-form-rel">اماده همکاری با مرکز هستید؟</h3>
          <div class="form-group">
          <input name="email" type="text" class="input-form-rel" placeholder="ایمیل خود راوارد کنید" />
          <button class="btn-submit" type="submit">شروع کنید</button>
        </div>
      </form>
      </div>
      <div class="rokcet-holder">
        <div class="original-roket"><img src="<?=fullBaseUrl()?>/image/rocket.png"></div>
        <div class="cloud-shape"><img src="<?=fullBaseUrl()?>/image/cloud-shape.png"></div>
      </div>
    </div>
  </header>
  <section class="box-component">
    <header class="header-component">
      <h6 class="min-title">خدمات ما</h6>
      <h2 class="main-title">خدمات برجسته مرکز فناوری</h2>
    </header>
    <div class="container-box slider-owner">
      <ul class="grid grid-line grid-khadamat">
<!--        <li class="grid-item grid-khadamat-item">-->
<!--          <div class="icon-item">-->
<!--            <img src="--><?//=baseUrl()?><!--/image/icons/email-icon.png" />-->
<!--          </div>-->
<!--          <h4 class="title-item">سرویس ایمیل</h4>-->
<!--          <p class="describe">ایمیل دانشگاه صرفا برای ارتباط داشتن دانشجو با ادمین اس-->
<!--            ت ایمیل دانشگاه صرفا برای ارتباط داشتن دانشجو با ادمین است</p>-->
<!--          <a class="btn" href="/">ورود به سامانه</a>-->
<!--        </li>-->


        <?for($i=0;$i<count($servicesRecord);$i++) {?>

          <li class="grid-item grid-khadamat-item">
            <div class="icon-item">
              <img src="<?=baseUrl()?>/image/icons/<?=$servicesRecord[$i]['thumbnail']?>.png" />
            </div>
            <h4 class="title-item"><?=$servicesRecord[$i]['title']?></h4>
            <p class="describe"> <?=$servicesRecord[$i]['detail']?></p>
            <a class="btn" href="http://<?=$servicesRecord[$i]['link']?>">ورود به سامانه</a>
          </li>

        <? } ?>
        <!-- <li class="grid-item grid-khadamat-item">
            <div class="icon-item">
                <img src="../src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
            </div>
            <h4 class="title-item">سرویس ایمیل</h4>
            <p class="describe">ایمیل دانشگاه صرفا برای ارتباط داشتن دانشجو با ادمین است ایمیل دان
                شگاه صرفا برای ارتباط داشتن دانشجو با ادمین است</p>
            <a class="btn" href="/">ورود به سامانه</a>
        </li>
        <li class="grid-item grid-khadamat-item">
            <div class="icon-item">
                <img src="../src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
            </div>
            <h4 class="title-item">سرویس ایمیل</h4>
            <p class="describe">ایمیل دانشگاه صرفا برای ارتباط داشتن دانشجو با ادمین است ایمیل دانشگا
                ه صرفا برای ارتباط داشتن دانشجو با ادمین است</p>
            <a class="btn" href="/">ورود به سامانه</a>
        </li>
        <li class="grid-item grid-khadamat-item">
            <div class="icon-item">
                <img src="../src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
            </div>
            <h4 class="title-item">سرویس ایمیل</h4>
            <p class="describe">ایمیل دانشگاه صرفا برای ارتباط داشتن دانشجو با ادمین است ایمیل دان
                شگاه صرفا برای ارتباط داشتن دانشجو با ادمین است</p>
            <a class="btn" href="/">ورود به سامانه</a>
        </li>
        <li class="grid-item grid-khadamat-item">
            <div class="icon-item">
                <img src="../src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
            </div>
            <h4 class="title-item">سرویس ایمیل</h4>
            <p class="describe">ایمیل دانشگاه صرفا برای ارتباط داشتن دانشجو با ادمین است ایمیل دان
                شگاه صرفا برای ارتباط داشتن دانشجو با ادمین است</p>
            <a class="btn" href="/">ورود به سامانه</a>
        </li> -->
      </ul>
      <!-- <div class="box-btn-container">
          <ul class="btns-box">
              <li></li>
              <li class="active"></li>
              <li></li>
              <li></li>
          </ul>
      </div>
      <span class="btn-slider left-btn">left</span>
      <span class="btn-slider right-btn">right</span> -->
    </div>
  </section>
  <section class="box-component">
    <header class="header-component">
      <h6 class="min-title">وضعیت سرور</h6>
      <h2 class="main-title">وضعیت سامانه های دانشگاه</h2>
    </header>
    <div class="container-box">
      <div class="box-gozaresh-server">
        <div class="side-pic">
          <div class="side-pic-holder">
            <img src="<?=baseUrl()?>/image/icons/server-status.png" />
          </div>
        </div>
        <div class="side-content">
          <ul class="box-ditale-gozaresh-server">
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-ok"></span>-->
<!--                  <span class="title-server">سامانه جامع دانشگاهی گلستان</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه کتابخانه</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه معرفی اعضای هیات علمی</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه متمرکز فرم ها</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه رزرو بلیط آمفی تئاتر</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه نشریات دانشگاه</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">پرتال دانشجویی صندوق رفاه</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه ارتباط مستمر با وزرات علوم</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه نشریات دانشجویی</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه ی پرداخت اینترنتی</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه ثبت نام اردوهای دانشجویی</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه اتوماسیون اداری</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه انبار</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه ورود و خروج پرسنل</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه فیش حقوقی</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه فیش حقوقی بازنشستگان</span>-->
<!--                </a>-->
<!--              </div>-->
<!--            </li>-->
<!--            <li class="item-gozaresh-server">-->
<!--              <div class="box-shoable-gozaresh">-->
<!--                <a href="/">-->
<!--                  <span class="icon icon-bad"></span>-->
<!--                  <span class="title-server">سامانه تغذیه</span>-->
<!--                </a>-->
<!--              </div>-->
<!--              <span class="icon icon-checkbox checked hidden"></span>-->
<!--            </li>-->
          </ul>
          <div class="button-continer gozarsh-btn-container">
            <span class="btn btn-crimson" type="SELECT">گزارش خرابی</span>
          </div>
          <div class="button-continer d-none gozarsh-btn-container">
            <span class="btn btn-dangres" type="CANCEL">لغو درخواست</span>
            <span class="btn btn-success" type="SUBMIT">
                                <span class="spinner "></span>
                                <span class="btn-title">ارسال درخواست</span>
                            </span>
            <span class="btn btn-rebeccapurple" type="SUBMIT_ALL">
                                <span class="spinner "></span>
                                <span class="btn-title">بررسی همه سرویس ها</span>
                            </span>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="box-component news-comp">
    <header class="header-component">
      <h6 class="min-title">جدید ترین اخبار</h6>
      <h2 class="main-title">اخرین مقالات و اخبار مرکز فناوری اطلاعات</h2>
    </header>
    <div class="container-box slider-owner">
      <ul class="grid grid-line">

        <?for($i=0;$i<count($newsRecord);$i++) {?>
        <li class="grid-item">
          <a href="<?=fullNewsUrl($newsRecord[$i]['news_id'])?>" class="box-container">
            <div class="pic-holder">
              <img src="<?=fullImageUrl($newsRecord[$i]['thumbnail'])?>" />
            </div>
            <div class="ditale-holder">
              <span class="prev-title"><?=$newsRecord[$i]['author']?></span>
              <h4 class="title"><?=$newsRecord[$i]['title']?></h4>
              <p class="content"> <?=$newsRecord[$i]['description']?>
              </p>
            </div>
          </a>
        </li>
        <? } ?>

      </ul>
    </div>
  </section>
</section>
<!--foooter-->


<!--l-->
<script src="<?=baseUrl()?>/js/Header.js"></script>
<script src="<?=baseUrl()?>/js/Slider.js"></script>
<script src="<?=baseUrl()?>/js/Services-Handler.js"></script>
<script>
  const boxDitaleGozareshServer = $(".box-ditale-gozaresh-server");
  const API_SERVER_CONDITION = "https://localhost:5000/";
  const config = {
    responsive: {
      0: 1,
      800: 2,
      1200: 3,
    },
  };
  createMiniSliderOwner(config);
  startServerCondition(boxDitaleGozareshServer, API_SERVER_CONDITION);
</script>

<!--<script>-->
<!--  $(document).ready(function() {-->
<!--    $.ajax('/nit/mainpage/ajaxServerReq',{-->
<!--      type: 'post',-->
<!--      dataType: 'json',-->
<!--      success:function (data) {-->
<!--       // alert(data.server1);-->
<!---->
<!--      }-->
<!--    })-->
<!--  });-->
<!---->
<!--</script>-->