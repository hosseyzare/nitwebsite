<footer id='footer'>
  <div class="footer-box">
    <ul class="section-view">
      <li class="item-view">
        <h4 class="title">اطلاعات تماس</h4>
        <div class="contaniner">
          <ul class="contact-us">
            <li class="item-cu">
              <a href="">
                <div class="item-cu-box">
                  <div class="icon-box">
                    <img src="<?=fullBaseUrl()?>/image/emailfooter.png" />
                  </div>
                  <div class="ditale-box">
                    <span class="up-text">ایمیل</span>
                    <span class="down-text">pr@nit.ac.ir</span>
                  </div>
                </div>
              </a>
            </li>
            <li class="item-cu">
              <a href="">
                <div class="item-cu-box">
                  <div class="icon-box">
                    <img src="<?=fullBaseUrl()?>/image/home.png" />
                  </div>
                  <div class="ditale-box">
                    <span class="up-text">نشانی مرکز </span>
                    <span class="down-text">مازندران، بابل، خیابان شریعتی، دانشگاه صنعتی نوشیروانی بابل</span>
                  </div>
                </div>
              </a>
            </li>
            <li class="item-cu">
              <div class="item-cu-box">
                <div class="icon-box">
                  <img src="<?=fullBaseUrl()?>/image/phone-numbers-call.png" />
                </div>
                <div class="ditale-box">
                  <span class="up-text">شماره تماس</span>
                  <span class="down-text">۴-۰۱۱۳۲۳۳۲۰۷۱</span>
                  <div class="up-text">
<!--                    <ul class="relation-box-icons">-->
<!--                      <li class="item-relation">-->
<!--                        <a href="/">-->
<!--                          <span class="icon"></span>-->
<!--                        </a>-->
<!--                      </li>-->
<!--                      <li class="item-relation"><a href="/"><span class="icon"></span></a>-->
<!--                      </li>-->
<!--                      <li class="item-relation"><a href="/"><span class="icon"></span></a>-->
<!--                      </li>-->
<!--                      <li class="item-relation"><a href="/"><span class="icon"></span></a>-->
<!--                      </li>-->
<!--                    </ul>-->
                  </div>
                </div>
              </div>

            </li>
          </ul>
        </div>
      </li>
      <li class="item-view">
        <h4 class="title">دسترسی سریع</h4>
        <div class="contaniner">
          <ul class="box-list-container">
            <li>
              <ul class="box-list">
                <li class="item-list"><a href="">ورود</a></li>
                <li class="item-list"><a href="">صفحه ی اصلی</a></li>
                <li class="item-list"><a href="">کتابخانه ی مرکزی</a></li>
                <li class="item-list"><a href="">سامانه ی فرم ها</a></li>
                <li class="item-list"><a href="">تماس با ما</a></li>
              </ul>
            </li>
            <li>
              <ul class="box-list">
                <li class="item-list"><a href="http://localhost:8080/nit/page/aboutus">درباره ی ما</a></li>
                <li class="item-list"><a href="http://localhost:8080/nit/page/aboutcorp">معرفی مرکز</a></li>
                <li class="item-list"><a href="http://localhost:8080/nit/page/contactus">تماس با ما</a></li>
                <li class="item-list"><a href="http://localhost:8080/nit/page/aboutmanager">درباره ی مدیریت</a></li>
                <li class="item-list"><a href="/">سایت دانشگاه</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </li>
      <li class="item-view">
        <h4 class="title">گالری تصاویر</h4>
        <div class="contaniner">
          <ul class="galery-footer">
            <li class="galery-item">
              <a href="/">
                <img src="<?=fullImageUrl('thumbnail1')?>" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="<?=fullImageUrl('thumbnail1')?>" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="<?=fullImageUrl('thumbnail1')?>" />
              </a>
            </li>

            <li class="galery-item">
              <a href="/">
                <img src="<?=fullImageUrl('thumbnail1')?>" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="<?=fullImageUrl('thumbnail1')?>" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="<?=fullImageUrl('thumbnail1')?>" />
              </a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</footer>