
<header id='header'>
  <nav class="nav-bar">
    <div class="logo-container">
      <a class="log-box-holder">
        <img src="<?=baseUrl()?>/image/icons/BabolNoshirvaniU_logo.jpg" />
        <h3 class="logo-name">مرکز فناوری دانشگاه نوشیروانی</h3>
      </a>
    </div>
    <div class="menu-container Menu-Dummy-Data">
      <ul class="menu-bar">
        <li class="menu-bar-item">
          <div class="title"><a href="/">صفحه اصلی</a></div>
          <ul class="menu-bar-inner">
            <li class="inner-item-menu">
              <a href="/">inner item menu</a>
            </li>
          </ul>
        </li>
    </div>
    <div class="box-container">
      <ul class="icon-holder">
        <li class="icon-item">
          <input type="checkbox" class="pure-input-tik" id="search-box-pure-css" />
          <label class="icon icon-search c-pointer" for="search-box-pure-css"></label>
          <label for="search-box-pure-css">
            <div class="back-drop"></div>
          </label>
          <div class="search-over-box">
            <label class="icon icon-close c-pointer" for="search-box-pure-css"></label>
            <div class="form-search">
              <input type="text" placeholder="سرچ کنید" class="search-input" />
              <span class="icon icon-search search-input-btn"></span>
            </div>
          </div>
        </li>
        <li class="icon-item">
          <label class="menu-icon c-pointer" for="side-box-fixed-pure">
            <span></span>
            <span></span>
            <span></span>
          </label>
        </li>
      </ul>
      <input type="checkbox" class="pure-input-tik" id="side-box-fixed-pure" />
      <label for="side-box-fixed-pure">
        <div class="back-drop"></div>
      </label>
      <div class="side-box-fixed">
        <label class="icon icon-close c-pointer" for="side-box-fixed-pure"></label>
        <div class="container-box">
          hi
        </div>
      </div>
    </div>
  </nav>
</header>