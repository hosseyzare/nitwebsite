<?
dump($newsRecord);
dump($commentRecords);

?>
<section id='content'>
  <div class="container">
    <div class="news-container">
      <h1 class="title">مشاهده اخبار</h1>

    </div>
    <div class="comments-container">
      <h5 class="title-comment-box">نظرات</h5>

    </div>
  </div>
</section>

<script src="<?=baseUrl()?>/js/Header.js"></script>
<script src="<?=baseUrl()?>/js/TextAreaHandler.js"></script>
<script src="<?=baseUrl()?>/js/Models/Sender.js"></script>
<script src="<?=baseUrl()?>/js/Models/News.js"></script>
<script src="<?=baseUrl()?>/Comment-Tree/dummy-data.js"></script>
<script src="<?=baseUrl()?>/Comment-Tree/Comment.js"></script>
<script src="<?=baseUrl()?>/Comment-Tree/Comment-Tree.js"></script>

<script>
  const containerNews = $(".news-container");
  const p = window.location.origin + "/src/assets/images/uploads/news/DuYim5bW4AUrR30.jpg";
  singleDataDummy = {
    sender: {
      id: 1,
      username: "سجاد سیفی",
      pic: "",
    },

    id: 1,
    title: "<?=$newsRecord['title']?>",
    pic: p,
    miniDescribe: "<?=$newsRecord['description']?>",
    descescribe: "<?=$newsRecord['text']?>",
    likeCount: 5,
    commentCount: 5,
    isLike: false,
    comments: commentsDummy
  };

  const { sender, id, title, pic, miniDescribe, likeCount, commentCount, isLike, descescribe, comments } = singleDataDummy;

  var newsObj = new News(
    id,
    title,
    pic,
    miniDescribe,
    likeCount,
    commentCount,
    isLike,
    descescribe,
    comments,
    sender
  );
  newsObj.render();
  $(containerNews).append(newsObj.current);

  const containerComments = $(".comments-container");
  const API_COMMENT = "http://localhost:5550/comments";
  //comments
  const tree = new CommentTree(containerComments, null, true);
  //set handler for get or add data in server side
  tree.addServerHandler(API_COMMENT, newsObj.textBox, newsObj.sendComment);
  //render containerBase and inner to iner tree-comments
  tree.render()
  //parameters if true! set commens dummy date affter fetched api
  //parameters if false! set real commens date in server affter fetched api
  tree.getAllCommentsFromApi(true);

  textAreaHandler(newsObj.textBox,17,3,6);
</script>