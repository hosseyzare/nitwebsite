
  <section class="box-component news-comp">
    <header class="header-component">
      <h6 class="min-title">جدید ترین اخبار</h6>
      <h2 class="main-title">اخرین مقالات و اخبار ما برای مشتریان</h2>
    </header>
    <div class="container-box">

      <ul class="grid grid-line">

        <?

          $iMax =count($results);
          for($i=0;$i<$iMax;$i++){
        ?>

        <li class="grid-item">
          <a href="<?=fullNewsUrl($results[$i]['news_id'])?>" class="box-container">
            <div class="pic-holder">
              <img src="<?=fullImageUrl($results[$i]['thumbnail'])?>" />
            </div>
            <div class="ditale-holder">
              <span class="prev-title">نویسنده : بابای صفر</span>
              <h4 class="title"><?=$results[$i]['title']?></h4>
              <p class="content">
                <?=$results[$i]['description']?>
              </p>
            </div>
          </a>
        </li>
        <?}?>
      </ul>
      <br>

      <?=pagination('/blog/page/',2,'span_btn','span-deactive-btn',$pageIndex,$pageCount);?>
    </div>

  </section>
</section>
<footer id='footer'>
  <div class="footer-box">
    <ul class="section-view">
      <li class="item-view">
        <h4 class="title">اطلاعات تماس</h4>
        <div class="contaniner">
          <ul class="contact-us">
            <li class="item-cu">
              <a href="">
                <div class="item-cu-box">
                  <div class="icon-box">
                    <img src="/src/assets/images/icons/search-1767866-1502119.webp" />
                  </div>
                  <div class="ditale-box">
                    <span class="up-text">u-text</span>
                    <span class="down-text">sdssd-text</span>
                  </div>
                </div>
              </a>
            </li>
            <li class="item-cu">
              <a href="">
                <div class="item-cu-box">
                  <div class="icon-box">
                    <img src="/src/assets/images/icons/search-1767866-1502119.webp" />
                  </div>
                  <div class="ditale-box">
                    <span class="up-text">u-text</span>
                    <span class="down-text">sdssd-text</span>
                  </div>
                </div>
              </a>
            </li>
            <li class="item-cu">
              <div class="item-cu-box">
                <div class="icon-box">
                  <img src="/src/assets/images/icons/search-1767866-1502119.webp" />
                </div>
                <div class="ditale-box">
                  <span class="up-text">u-text</span>
                  <div class="up-text">
                    <ul class="relation-box-icons">
                      <li class="item-relation">
                        <a href="/">
                          <span class="icon"></span>
                        </a>
                      </li>
                      <li class="item-relation"><a href="/"><span class="icon"></span></a>
                      </li>
                      <li class="item-relation"><a href="/"><span class="icon"></span></a>
                      </li>
                      <li class="item-relation"><a href="/"><span class="icon"></span></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

            </li>
          </ul>
        </div>
      </li>
      <li class="item-view">
        <h4 class="title">دسترسی سریع</h4>
        <div class="contaniner">
          <ul class="box-list-container">
            <li>
              <ul class="box-list">
                <li class="item-list"><a href="">item</a></li>
                <li class="item-list"><a href="">item</a></li>
                <li class="item-list"><a href="">item</a></li>
              </ul>
            </li>
            <li>
              <ul class="box-list">
                <li class="item-list"><a href="">item</a></li>
                <li class="item-list"><a href="">item</a></li>
                <li class="item-list"><a href="">item</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </li>
      <li class="item-view">
        <h4 class="title">گالری تصاویر</h4>
        <div class="contaniner">
          <ul class="galery-footer">
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>

            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</footer>