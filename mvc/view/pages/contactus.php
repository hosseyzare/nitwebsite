<section id='content'>
  <div class="center-cetner-container">

    <div class="center-center-box">
      <h1 class="title-form">ثبت به حساب کاربری</h1>
      <form class="form" onsubmit="return false">
        <div class="form-group">
          <label for="fullname"> نام خود را وارد کنید</label>
          <input type="text" name="fullname" id="fullname" />
        </div>
        <div class="form-group">
          <label for="username">ایمیل خود را واید کنید</label>
          <input type="email" name="email" id="email" />
        </div>
        <div class="form-group">
          <label for="subject">عنوان پیام را وارد کنید</label>
          <input type="text" name="subject" id="subject" />
        </div>
        <div class="form-group">
          <label for="message">متن پیام را وارد کنید</label>
          <textarea name="message" id="message"></textarea>
        </div>
        <div class="form-group center-center-container"   style="padding:20px 0 0;" >
                            <span class="button-form btn-send-form" onclick="ContactUs()">
                                <span class="spinner  "></span>
                                <span class="btn-title">
                                    ارسال پیام
                                </span>
                            </span>
        </div>
      </form>
    </div>
    <div class="side">
      <div class="ditale-univecity">
        <div class="ditale-item-box">
          <span class="ditale-item-title">تلفن مرکز دانشگاه</span>
          <span class="split">:</span>
          <span class="ditale-item-value">۰۱۱۳۲۳۳۲۰۷۱-۴</span>
        </div>
        <div class="ditale-item-box">
          <span class="ditale-item-title">کد پستی</span>
          <span class="split">:</span>
          <span class="ditale-item-value">۷۳۱۱۳-۴۷۱۴۸</span>
        </div>
        <div class="ditale-item-box">
          <span class="ditale-item-title">پست الکترونیکی</span>
          <span class="split">:</span>
          <span class="ditale-item-value">pr@nit.ac.ir</span>
        </div>
      </div>
      <div class="map">
        <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1602.3542381618126!2d52.67998020601038!3d36.56114149851339!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8f88155391a6cb%3A0x381087fd25f45508!2sBabol%20Noshirvani%20University%20of%20Technology%20(BUT)!5e0!3m2!1sen!2snl!4v1607168337934!5m2!1sen!2snl"  frameborder="0"  allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </div>
    </div>
  </div>
</section>
<script src="<?=baseUrl()?>/js/Form.js"></script>
<script src="<?=baseUrl()?>/js/Header.js"></script>
<script>
  const fullname = document.querySelector("form #fullname");
  const email = document.querySelector("form #email");
  const subject = document.querySelector("form #subject");
  const message = document.querySelector("form #message");
  const API_SIGN_CONTACT_US = "http://localhost:5000/";
  function ContactUs() {
    if ($(".btn-send-form").hasClass("clicked"))
      return;
    if (fullname.value == "" || email.value == "" || subject.value == "" || message.value == "") {
      alert("لطفا تمامی فیلد ها را پر کنید");
      return;
    }
    const data = {
      fullname,
      email,
      subject,
      message
    };
    callServer({
      url: API_SIGN_CONTACT_US,
      data,
      type: "POST",
    }, 0);
  }
</script>