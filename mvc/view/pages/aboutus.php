
<section id='content'>
  <div class="table-hodler">
    <h3 class="title-table">مدیران مرکز خدمات کامپیوتری</h3>
    <div class="box-table">
      <div>
        <div class="top">نام و نام خانوادگی</div>
        <p>دکتر مهدی عمادی</p>
        <p>دکتر علی غلامی</p>
        <p>دکتر سید مهدی میرایمانی</p>
      </div>
      <div>
        <div class="top">سمت</div>
        <p>مدیریت مرکز</p>
        <p>مدیر خدمات شبکه و ارتباطات</p>
        <p>مدیر خدمات نرم افزاری</p>
      </div>
      <div>
        <div class="top">ایمیل</div>
        <p><a href="#">m.emadi@nit.ac.ir</a></p>
        <p><a href="#">gholamirudi@nit.ac.ir</a></p>
        <p><a href="#">mirimani@nit.ac.ir</a></p>
      </div>
      <div>
        <div class="top">آدرس دفتر</div>
        <p>ساختمان خوارزمی</p>
        <p>ساختمان خوارزمی</p>
        <p>ساختمان آزمایشگاه ماشین های الکتریکی</p>
      </div>
      <div>
        <div class="top">داخلی</div>
        <p>۱۱۷۵</p>
        <p>۱۱۷۴</p>
        <p>۱۴۳۶</p>
      </div>
    </div>
    <h3 class="title-table">خدمات محاسبات سریع (HPC)</h3>
    <div  class="box-table">
      <div>
        <div class="top">نام و نام خانوادگی</div>
        <p>علی ولی زاده</p>
      </div>
      <div>
        <div class="top">سمت</div>
        <p>کارشناس سامانه محاسبات سریع</p>
      </div>
      <div>
        <div class="top">ایمیل</div>
        <p><a href="#">ali_valizadeh@nit.ac.ir</a></p>
      </div>
      <div>
        <div class="top">آدرس دفتر</div>
        <p>ساختمان رازی، طبقه همکف، انتهای غربی راهرو، مرکز  خدمات محاسبات سریع؛</p>
      </div>
      <div>
        <div class="top">داخلی</div>
        <p>۱۱۷۲</p>
      </div>
    </div>
    <h3 class="title-table">خدمات اینترنت و شبکه</h3>
    <div  class="box-table">
      <div>
        <div class="top">نام و نام خانوادگی</div>
        <p>همت فیروزمندی</p>
        <p>حسین واحدی</p>
      </div>
      <div>
        <div class="top">سمت</div>
        <p>کارشناس مسئول شبکه</p>
        <p>پشتیبان شبکه و اینترنت</p>
      </div>
      <div>
        <div class="top">ایمیل</div>
        <p><a href="#">hf@nit.ac.ir</a></p>
        <p><a href="#">hvahedi@nit.ac.ir</a></p>
      </div>
      <div>
        <div class="top">آدرس دفتر</div>
        <p>ساختمان خوارزمی، واحد مدیریت شبکه و اینترنت؛</p>
        <p>ساختمان خوارزمی، واحد مدیریت شبکه و اینترنت؛</p>
      </div>
      <div>
        <div class="top">داخلی</div>
        <p>۱۱۷۴</p>
        <p>۱۱۷۴</p>
      </div>
    </div>
    <h3 class="title-table">خدمات تعمیرات سخت‌افزار و نرم‌افزار</h3>
    <div class="box-table">
      <div>
        <div class="top">نام و نام خانوادگی</div>
        <p>عباس هدایتی</p>
        <p>محمد دهزاد</p>
      </div>
      <div>
        <div class="top">سمت</div>
        <p>کارشناس سخت افزار</p>
        <p>پشتیبان نرم افزار</p>
      </div>
      <div>
        <div class="top">ایمیل</div>
        <p><a href="#">abbas@nit.ac.ir</a></p>
        <p><a href="#">dehzad@nit.ac.ir</a></p>
      </div>
      <div>
        <div class="top">آدرس دفتر</div>
        <p>ساختمان خوارزمی، واحد سخت افزار؛</p>
        <p>ساختمان خوارزمی، واحد نرم افزار؛</p>
      </div>
      <div>
        <div class="top">داخلی</div>
        <p>۱۱۷۷</p>
        <p>۱۱۷9</p>
      </div>
    </div>
    <h3 class="title-table">خدمات سایت کارشناسی، ارشد و دکتری</h3>
    <div class="box-table">
      <div>
        <div class="top">نام و نام خانوادگی</div>
        <p>مجید محمودی عالمی</p>
        <p>فاطمه اسکندری</p>
        <p>همت فیروزمندی</p>
      </div>
      <div>
        <div class="top">سمت</div>
        <p>مسئول سایت کارشناسی</p>
        <p>مسئول سایت کارشناسی‌ارشد</p>
        <p>مسئول سایت دکتری</p>
      </div>
      <div>
        <div class="top">ایمیل</div>
        <p><a href="#">mahmoodi@nit.ac.ir</a></p>
        <p><a href="#">f.eskandari@nit.ac.ir</a></p>
        <p><a href="#">hf@nit.ac.ir</a></p>
      </div>
      <div>
        <div class="top">آدرس دفتر</div>
        <p>ساختمان خوارزمی، واحد مدیریت سایت کارشناسی ؛</p>
        <p>ساختمان خوارزمی، واحد مدیریت سایت کارشناسی ارشد؛</p>
        <p>ساختمان رازی، طبقه فوقانی، سایت دکتری؛</p>
      </div>
      <div>
        <div class="top">داخلی</div>
        <p>۱۱۷۱</p>
        <p>۱۱۰۶۵</p>
        <p>۱۱۷۸</p>
      </div>
    </div>
    <h3 class="title-table">خدمات وب سایت دانشگاه</h3>
    <div class="box-table">
      <div>
        <div class="top">نام و نام خانوادگی</div>
        <p>مهرنوش نظری</p>
        <p>علی حاجی عباسی</p>
      </div>
      <div>
        <div class="top">سمت</div>
        <p>کارشناس مسئول وبه</p>
        <p>پشتیبان فنی وب سایت ها</p>
      </div>
      <div>
        <div class="top">ایمیل</div>
        <p><a href="#">m.nazari@nit.ac.ir</a></p>
        <p><a href="#">ah@nit.ac.ir</a></p>
      </div>
      <div>
        <div class="top">آدرس دفتر</div>
        <p>ساختمان مرکزی، طبقه سوم، واحد وب؛</p>
        <p>ساختمان مرکزی، طبقه سوم، واحد وب</p>
      </div>
      <div>
        <div class="top">داخلی</div>
        <p>۱۱۵۷</p>
        <p>۱۱۴۰</p>
      </div>
    </div>
    <h3 class="title-table">خدمات سیستم مدیریت آموزش (گلستان) و سامانه آموزش مجازی (سمیا)</h3>
    <div class="box-table">
      <div>
        <div class="top">نام و نام خانوادگی</div>
        <p>سید محمد ربیعی</p>
        <p>راضیه اسفندیاری</p>
      </div>
      <div>
        <div class="top">سمت</div>
        <p>پشتیبان سیستم گلستان</p>
        <p>پشتیبان سیستم سمیا</p>
      </div>
      <div>
        <div class="top">ایمیل</div>
        <p><a href="#">smrabiee@nit.ac.ir</a></p>
        <p><a href="#">r.esfandiari@nit.ac.ir</a></p>
      </div>
      <div>
        <div class="top">آدرس دفتر</div>
        <p> معاونت دانشجویی، واحد پشتیبانی سیستم گلستان؛</p>
        <p> معاونت دانشجویی، واحد پشتیبانی سیستم سمیا؛</p>
      </div>
      <div>
        <div class="top">داخلی</div>
        <p>۱۱۷۰</p>
        <p>۱۰۰۵</p>
      </div>
    </div>
  </div>
</section>

<script src="<?=baseUrl()?>/js/Header.js"></script>