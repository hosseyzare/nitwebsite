
<header id='header'>
  <nav class="nav-bar">
    <div class="logo-container">
      <a class="log-box-holder">
        <img src="../src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
        <span class="logo-name">مرکز فناوری دانشگاه نوشیروانی</span>
      </a>
    </div>
    <div class="menu-container">
      <ul class="menu-bar">
        <li class="menu-bar-item">
          <div class="title"><a href="/">صفحه اصلی</a></div>
          <ul class="menu-bar-inner">
            <li class="inner-item-menu">
              <a href="/">inner item menu</a>
            </li>
          </ul>
        </li>
        <li class="menu-bar-item">
          <div class="title"><a href="/">خدمات مرکز</a></div>
          <ul class="menu-bar-inner">
            <li class="inner-item-menu">
              <a href="/">inner item menu</a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="box-container">
      <ul class="icon-holder">
        <li class="icon-item"><span class="icon icon-search"></span></li>
        <li class="icon-item">
          <div class="menu-icon">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </li>
      </ul>
    </div>
  </nav>
</header>
<section id='content'>
  <div class="center-cetner-container">
    <div class="center-center-box">
      <h1 class="title-form">دعوت به همکاری</h1>
      <form class="form" onsubmit="return false">
        <div class="form-row">
          <div class="form-group">
            <label for="firstname"> نام خود را وارد کنید</label>
            <input type="text" name="firstname" id="firstname" />
          </div>
          <div class="form-group">
            <label for="lastname">نام خانوادگی خود را وارد کنید</label>
            <input type="text" name="lastname" id="lastname" />
          </div>
        </div>
        <div class="form-row">
          <div class="form-group">
            <label for="email">رایان نامه الکتریکی را وارد کنید</label>
            <?if($isEmailFilled){?>
              <input type="email" name="email" id="email" value="<?=$email?>"/>
            <? }else{ ?>
              <input type="email" name="email" id="email" />
            <? } ?>
          </div>
          <div class="form-group">
            <label for="phone">شماره تماس خود را وارد نمایید</label>
            <input type="text" name="phone" id="phone" />
          </div>
        </div>
        <div class="form-group">
          <label for="favarits">زمینه مورد علاقه خورد رانام ببرید</label>
          <div class="tab-box-favorit"></div>
          <!-- <textarea name="favarits" id="favarits" class="mini"></textarea> -->
        </div>
        <div class="form-group">
          <label for="message">متن پیام را وارد کنید</label>
          <textarea name="message" id="message"></textarea>
        </div>
        <div class="form-group center-center-container" style="padding:20px 0 0;">
                        <span class="button-form btn-send-form" onclick="AddCooperation()">
                            <span class="spinner  "></span>
                            <span class="btn-title">
                                درخواست برای همکاری
                            </span>
                        </span>
        </div>
      </form>
    </div>
  </div>
</section>
<footer id='footer'>
  <div class="footer-box">
    <ul class="section-view">
      <li class="item-view">
        <h4 class="title">اطلاعات تماس</h4>
        <div class="contaniner">
          <ul class="contact-us">
            <li class="item-cu">
              <a href="">
                <div class="item-cu-box">
                  <div class="icon-box">
                    <img src="/src/assets/images/icons/search-1767866-1502119.webp" />
                  </div>
                  <div class="ditale-box">
                    <span class="up-text">u-text</span>
                    <span class="down-text">sdssd-text</span>
                  </div>
                </div>
              </a>
            </li>
            <li class="item-cu">
              <a href="">
                <div class="item-cu-box">
                  <div class="icon-box">
                    <img src="/src/assets/images/icons/search-1767866-1502119.webp" />
                  </div>
                  <div class="ditale-box">
                    <span class="up-text">u-text</span>
                    <span class="down-text">sdssd-text</span>
                  </div>
                </div>
              </a>
            </li>
            <li class="item-cu">
              <div class="item-cu-box">
                <div class="icon-box">
                  <img src="/src/assets/images/icons/search-1767866-1502119.webp" />
                </div>
                <div class="ditale-box">
                  <span class="up-text">u-text</span>
                  <div class="up-text">
                    <ul class="relation-box-icons">
                      <li class="item-relation">
                        <a href="/">
                          <span class="icon"></span>
                        </a>
                      </li>
                      <li class="item-relation"><a href="/"><span class="icon"></span></a>
                      </li>
                      <li class="item-relation"><a href="/"><span class="icon"></span></a>
                      </li>
                      <li class="item-relation"><a href="/"><span class="icon"></span></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

            </li>
          </ul>
        </div>
      </li>
      <li class="item-view">
        <h4 class="title">دسترسی سریع</h4>
        <div class="contaniner">
          <ul class="box-list-container">
            <li>
              <ul class="box-list">
                <li class="item-list"><a href="">item</a></li>
                <li class="item-list"><a href="">item</a></li>
                <li class="item-list"><a href="">item</a></li>
              </ul>
            </li>
            <li>
              <ul class="box-list">
                <li class="item-list"><a href="">item</a></li>
                <li class="item-list"><a href="">item</a></li>
                <li class="item-list"><a href="">item</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </li>
      <li class="item-view">
        <h4 class="title">گالری تصاویر</h4>
        <div class="contaniner">
          <ul class="galery-footer">
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>

            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
            <li class="galery-item">
              <a href="/">
                <img src="/src/assets/images/icons/BabolNoshirvaniU_logo.jpg" />
              </a>
            </li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</footer>
<script src="<?=baseUrl()?>/js/Form.js"></script>
<script src="<?=baseUrl()?>/js/text-box-tab.js"></script>
<script>
  const boxs = document.querySelectorAll(".tab-box-favorit");
  addTextBoxTab(boxs);
</script>
<script>
  const firstname = document.querySelector("form #firstname");
  const lastname = document.querySelector("form #lastname");
  const email = document.querySelector("form #email");
  const phone = document.querySelector("form #phone");
  const message = document.querySelector("form #message");
  const API_SIGN_IN = "http://localhost:8080/nit/career/addcareerbyregex";
  function AddCooperation() {
    if ($(".btn-send-form").hasClass("clicked"))
      return;
    if (firstname.value == "" || lastname.value == "") {
      alert("لطفا تمامی فیلد ها را پر کنید");
      return;
    }
    const interest = getAllDataTextTabBoxs()[0];
    var formData = {
      'email': email.value,
      'firstname':firstname.value,
      'lastname': lastname.value,
      'phonenumber': phone.value,
      'interest': interest,
      'text': message.value
    };

    console.log(formData);
    callServer({
      url: '/nit/career/addcareerbyregex',
      type: 'post',
      data: formData,
      success:(res)=>{
        console.log(res);
      },
      error:(e)=>{
        //alert(e.responseText);
        document.body.innerHTML = (e.responseText);
      }
    }, 0);
  }
</script>