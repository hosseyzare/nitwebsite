<?php
function hr($return = false){
  if ($return){
    return "<hr>\n";
  } else {
    echo "<hr>\n";
  }
}

function br($return = false){
  if ($return){
    return "<br>\n";
  } else {
    echo "<br>\n";
  }

}

function dump($var, $return = false){
  if (is_array($var)){
    $out = print_r($var, true);
  } else if (is_object($var)) {
    $out = var_export($var, true);
  } else {
    $out = $var;
  }

  if ($return){
    return "\n<pre style='direction: ltr'>$out</pre>\n";
  } else {
    echo "\n<pre style='direction: ltr'>$out</pre>\n";
  }
}

function getCurrentDateTime(){
  return date("Y-m-d H:i:s");
}

function encryptPassword($password){
  global $config;
  return md5($password . $config['salt']);
}

function getFullUrl(){
  $fullurl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  return $fullurl;
}

function getRequestUri(){
  return $_SERVER['REQUEST_URI'];
}

function baseUrl(){
  global $config;
  return $config['base'];
}

function fullBaseUrl(){
  global $config;
  return 'http://' . $_SERVER['HTTP_HOST'] . $config['base'];
}

function strhas($string, $search, $caseSensitive = false){
  if ($caseSensitive){
    return strpos($string, $search) !== false;
  } else {
    return strpos(strtolower($string), strtolower($search)) !== false;
  }
}

function message($type, $message, $mustExit = false) {
  $data['message'] = $message;
  View::render("/message/$type.php", $data);
  if ($mustExit){
    exit;
  }


}


function fullImageUrl($imagename){
  return baseUrl() .'/image/thumbnail/' . $imagename .'.jpg';

}

function fullNewsUrl($newsId){
  return baseUrl() .'/blog/news/' . $newsId ;

}


 function pagination($url,$showCount,$activeClass,$deactiveClass,$currentPageIndex,$pageCount){
  ob_start();
  ?>
  <a href="<?=baseUrl()?><?=$url?>1" class="<?=$activeClass?>">1</a>
  <span>...</span>
  <? for ($i=$currentPageIndex-$showCount;$i<=$currentPageIndex+$showCount;$i++){?>
    <? if ($i<=1){continue;}?>
    <? if ($i>=$pageCount){continue;}?>
    <? if ($i == $currentPageIndex){ ?>
      <span  class="<?=$deactiveClass?>"><?=$i?></span>
    <? }else{ ?>
      <a href="<?=baseUrl()?><?=$url?><?=$i?>" class="<?=$activeClass?>"><?=$i?></a>
    <?}?>
  <?}?>
  <span>...</span>
  <a href="<?=baseUrl()?><?=$url?><?=$pageCount?>" class="<?=$activeClass?>"><?=$pageCount?></a>
<?

  $output = ob_get_clean();
  return $output;
 }
