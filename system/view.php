<?php
class View {
  public static function render($filePath, $data = array()){
    extract($data);

    ob_start();
    require_once("mvc/view/footer/simpleFooter.php");
    $footer = ob_get_clean();

    ob_start();
    //header
    require_once("mvc/view/headers/simpleHeader.php");
    //main content
    require_once("mvc/view" . $filePath);
    //footer

    $content = ob_get_clean();

    require_once("theme/default.php");
  }
  public static function rendershowAllAdminPage($filePath, $data = array()){
    extract($data);

    ob_start();
    require_once("mvc/view" . $filePath);
    $content = ob_get_clean();

    require_once("theme/showAllAdminPage.php");
  }
  public static function renderaddAdminPage($filePath, $data = array()){
    extract($data);

    ob_start();
    require_once("mvc/view" . $filePath);
    $content = ob_get_clean();

    require_once("theme/addsAdminPage.php");
  }
  public static function renderCareerPage($filePath, $data = array()){
    extract($data);

    ob_start();
    require_once("mvc/view" . $filePath);
    $content = ob_get_clean();

    require_once("theme/career.php");
  }
  public static function renderCheckMessageAdminPage($filePath, $data = array()){
    extract($data);

    ob_start();
    require_once("mvc/view" . $filePath);
    $content = ob_get_clean();

    require_once("theme/checkMessageAdminPage.php");
  }
  public static function renderLoginPage($filePath, $data = array()){
    extract($data);

    ob_start();
    require_once("mvc/view" . $filePath);
    $content = ob_get_clean();

    require_once("theme/checkMessageAdminPage.php");
  }
  public static function renderSingleNews($filePath, $data = array()){
    extract($data);

    ob_start();
    require_once("mvc/view/footer/simpleFooter.php");
    $footer = ob_get_clean();

    ob_start();
    //header :
    require_once("mvc/view/headers/simpleHeader.php");

    //main :
    require_once("mvc/view" . $filePath);
    $content = ob_get_clean();

    require_once("theme/singleNews.php");
  }
  public static function renderPages($filePath, $data = array()){
    extract($data);

    ob_start();
    require_once("mvc/view/footer/simpleFooter.php");
    $footer = ob_get_clean();

    ob_start();
    //header :
    require_once("mvc/view/headers/simpleHeader.php");

    //main :
    require_once("mvc/view" . $filePath);
    $content = ob_get_clean();

    require_once("theme/pages.php");
  }
}