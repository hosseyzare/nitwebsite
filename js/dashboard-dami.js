let dashboardMenu = [
    {
        title: "اخبار",
        baseRoute: "/nit/",
        options: [
            { title: "افزودن خبر", href: "dashboard/addnews" },
            { title: "همه اخبار", href: "dashboard/showallnews" }
        ]
    },
    {
        title: "خدمات",
        baseRoute: "/nit/",
        options: [
            { title: "افزودن خدمت", href: "dashboard/addServices" },
            { title: "همه ی خدمات", href: "dashboard/showAllServices" }
        ]
    },
    {
        title: "راهنما",
        baseRoute: "/nit/",
        options: [
            { title: "افزودن راهنمایی", href: "dashboard/addHelps" },
            { title: "همه راهنمایی ها", href: "dashboard/showAllHelps" }
        ]
    }
    ,
    {
        title: "پیام ها",
        baseRoute: "/nit/",
        options: [
            { title: "درخواست های همکاری", href: "dashboard/ShowAllCareerMessage" },
            { title: "تماس با ما", href: "dashboard/ShowAllContactToUsMessage" },
            { title: "نظرات", href: "dashboard/ShowAllComments" }
        ]
    }
    ,
    {
        title: "تنظیمات",
        baseRoute: "/nit/",
        options: [
            { title: "ترجمه (چند زبانه)", href: "dashboard/stillworking" },
            { title: "راه اندازی اولیه", href: "dashboard/stillworking" }
        ]
    }
];
const DashboardMenuCDD = document.querySelector(".Dashboard-Menu-CDD");
console.log(dashboardMenu, DashboardMenuCDD);
createMenu(DashboardMenuCDD);
function createMenu(parentNode) {
    if (!parentNode)
        return;
    //parentNode.classList.add("Dashboard-Menu-CDD");
    const containerDashboradItem = document.createElement("ul");
    containerDashboradItem.classList
        .add("container-dashborad-item");

    dashboardMenu.forEach(
        (item, i) => {
            const itemDashborad = createItemMenu(item);
            containerDashboradItem.appendChild(itemDashborad);
        }
    );
    parentNode.appendChild(containerDashboradItem);

};

function createItemMenu({ title, options, baseRoute }) {
    seletedIdnex = options.findIndex(
        op =>
            document.location.href
            .toLocaleLowerCase().includes(
                op.href.toLocaleLowerCase()
            )
    );
    let itemDashborad = document.createElement("li");
    itemDashborad.classList.add("item-dashborad");
    let titleH4 = document.createElement("h4");
    titleH4.classList.add("title-item");
    titleH4.innerHTML = title;
    console.log(title);
    let containerSubItms = document.createElement("ul");
    containerSubItms.classList.add("container-sub-itms");
    let mapedItems = createOptions(options, baseRoute);
    if (seletedIdnex != -1) {
        itemDashborad.classList.add("active");
        mapedItems[seletedIdnex].classList.add("active-sub");
    }
    mapedItems.forEach(op => containerSubItms.appendChild(op));
    console.log(containerSubItms.style.height);
    itemDashborad.appendChild(titleH4);
    itemDashborad.appendChild(containerSubItms);

    titleH4.addEventListener('click', (e) => clickedItem(itemDashborad));
    return itemDashborad;
}
function createOptions(options, baseRoute) {
    return [...options].map((op, i) => {
        const subItms = document.createElement("li");
        subItms.classList.add("sub-itms");
        const a = document.createElement("a");
        a.innerHTML = op.title;
        a.href = baseRoute + op.href;
        subItms.appendChild(a);
        return subItms;
    });
}
function clickedItem(itemDashboard) {
    if (itemDashboard.classList.contains("active")) {
        itemDashboard.classList.remove("active");
    } else {
        itemDashboard.classList.add("active");
    }
}