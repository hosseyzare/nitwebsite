const keyCode = {
    ENTER: 13,
    BACKSPACE:8,
};
let allDataTextTabBox = [];
let allOfTabTextBox = [];
const baseClassTextTabBox = "text-box-tab";
const activeEmptyText = () => document.querySelector(".text-box-tab.active .empty-text");
const isEmptyActiveText = () => (!activeEmptyText() || activeEmptyText().innerHTML.length == 0);
const activeTabTextBox = () => document.querySelector(".text-box-tab.active");
const clickedTextTabBox = () => document.querySelector(".text-box-tab .MOUSE_CLICKED_CHECK_TAB_BOX");
const indexOfTabTextBox = (box) => allOfTabTextBox.findIndex(b => b == box);
const isOutOfTabBox = (container) => container.classList.contains(baseClassTextTabBox) && container.classList.contains("active");

function addTextBoxTab(containers = []) {
    containers.forEach(
        (con) => {
            handleextBoxTabContainer(con);
            allDataTextTabBox.push([]);
        }
    );

    allOfTabTextBox = document.querySelectorAll(".text-box-tab");

}
function handleextBoxTabContainer(container) {
    const tabEntityEnd = document.createElement("span");
    tabEntityEnd.classList.add("tab-entity-end");
    const lineCursor = document.createElement("span");
    lineCursor.classList.add("line-cursor");
    lineCursor.innerHTML = "|";
    const emptyText = document.createElement("span");
    emptyText.classList.add("empty-text");

    tabEntityEnd.appendChild(emptyText);
    tabEntityEnd.appendChild(lineCursor);

    container.appendChild(tabEntityEnd);
    container.classList.add("text-box-tab");
    //enter to box
    container.addEventListener("mousedown", enterToTextBoxTab);
    //out of box
    container.addEventListener("mouseout", outOfTextBoxTab);
}
function enterToTextBoxTab(e) {
    if (!this.classList.contains("active")) {
        const box = activeTabTextBox();
        if (box)
            box.classList.remove("active");
        this.classList.add("active");

    }
    const emp = activeEmptyText();
    if (emp && emp.innerHTML.length > 0) {
        createTabEntity();
        return;
    }

    document.addEventListener("keyup", enteredByKeyboardToTabBox);
}
function outOfTextBoxTab(e) {
    const emt = activeEmptyText();
    if (emt && emt.innerHTML.length > 0) {
        createTabEntity();
    }
    this.classList.remove("active");
    document.removeEventListener("keyup", enteredByKeyboardToTabBox);
}
function enteredByKeyboardToTabBox(e) {
    const emt = activeEmptyText();
    console.log(e.keyCode);
    switch (e.keyCode) {
        case keyCode.ENTER:
            console.log("create Enter");
            createTabEntity();
            return;
        case keyCode.BACKSPACE:
            emt.innerHTML = emt.innerHTML.substr(0,emt.innerHTML.length-1);
            break;
        default:
            emt.innerHTML += e.key;
    }
}
function createTabEntity() {
    const empAct = activeEmptyText();
    if (isEmptyActiveText())
        return;

    const title = empAct.innerHTML;
    const tabEntity = getDomTabEntity(title);
    const box = activeTabTextBox();
    const last = box.lastChild;
    //indexBox = indexOfTabTextBox(box);

    empAct.innerHTML = "";
    box.appendChild(tabEntity);
    box.appendChild(last);
}
function getDomTabEntity(title) {
    const tabEntity = document.createElement("span");
    tabEntity.classList.add("tab-entity");
    const textTabEntity = document.createElement("span");
    textTabEntity.classList.add("text-tab-entity");
    textTabEntity.innerHTML = title;
    const removeTabEntity = document.createElement("span");
    removeTabEntity.classList.add("close-tab-entity");

    //remove entity
    removeTabEntity.addEventListener("click", removeEntityInTabBox);

    tabEntity.appendChild(textTabEntity);
    tabEntity.appendChild(removeTabEntity);

    return tabEntity;
}

function removeEntityInTabBox(e) {
    const entity = this.parentNode;
    entity.classList.add("remove");
    this.removeEventListener("click",removeEntityInTabBox);
    setTimeout(() => activeTabTextBox().removeChild(entity), 400);
}
function getAllDataTextTabBoxs() {
    allDataTextTabBox = [];
    for (let i = 0; i < allOfTabTextBox.length; i++) {
        allOfTabTextBox[i].classList.add("is-select-for-get-data");
        const tabsOfBoxNodes = document.querySelectorAll(".is-select-for-get-data .tab-entity .text-tab-entity");
        allOfTabTextBox[i].classList.remove("is-select-for-get-data");

        const tabsOfBox = [...tabsOfBoxNodes].map(tab => tab.innerHTML);

        allDataTextTabBox.push(tabsOfBox);
    }
    return allDataTextTabBox;
}