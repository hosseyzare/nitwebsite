let __API_SERVICES_HANDLER__ = "";
const serverContitionDataDummy = [
    { id: 1, title: "سامانه جامع دانشگاهي گلستان", href: "/", condition: true },
    { id: 2, title: "سامانه كتابخانه", href: "/", condition: false },
    { id: 3, title: "سامانه تغذيه", href: "/", condition: true },
    { id: 4, title: "سامانه معرفي اعضاي هيأت علمي", href: "/", condition: true },
    { id: 5, title: "سامانه متمركز فرم ها", href: "/", condition: true },
    { id: 6, title: "سامانه رزرو بليط آمفي تئاتر", href: "/", condition: true },
    { id: 7, title: "سامانه نشريات دانشگاه", href: "/", condition: true },
    { id: 8, title: "پرتال دانشجويي صندوق رفاه", href: "/", condition: false },
    { id: 9, title: "سامانه ارتباطات مستمر با وزارت علوم", href: "/", condition: true },
    { id: 10, title: "سامانه نشريات دانشجويي", href: "/", condition: true },
    { id: 11, title: "سامانه پرداخت اينترنتي", href: "/", condition: true },
    { id: 12, title: "سامانه ثبت نام اردوهاي دانشجويي", href: "/", condition: true },
    { id: 13, title: "سامانه اتوماسيون اداري", href: "/", condition: true },
    { id: 14, title: "سامانه انبار", href: "/", condition: true },
    { id: 15, title: "سامانه ورود و خروج پرسنل", href: "/", condition: true },
    { id: 16, title: "سامانه فيش حقوقي", href: "/", condition: true },
    { id: 17, title: "سامانه فيش حقوقي بازنشستگان", href: "/", condition: false },
];
function startServerCondition(box,api){
    __API_SERVICES_HANDLER__=api;
    //api getServerCondition();
    createNodesServerCondition(box,serverContitionDataDummy);
}
function getServerCondition() {
    $.ajax({
        url: __API_SERVICES_HANDLER__,
        data: {},
        type: "GET",
        contentType: "application/json",
        success: function (res) {

        },
        error: function (e) {

        }
    });
}
function createNodesServerCondition(box,data = []) {
    const nodesServerCondition = data.map(d => createOneNodeServerCondition(d));
    
    $(box).empty().append(nodesServerCondition);
}
let enableClickItemServices = true;
function createOneNodeServerCondition({ id, title, href, condition }) {
    const itemGozareshServer = $("<li>").addClass("item-gozaresh-server").attr({ servicesId: id });
    const boxShoableGozaresh = $("<div>").addClass("box-shoable-gozaresh")
    const checkbox = $("<sapn>").addClass("icon").addClass("icon-checkbox").addClass("hidden");

    const ancore = $("<a>").attr({ href });

    const icon = $("<span>").addClass("icon").addClass(condition ? "icon-ok" : "icon-bad");
    const titleServer = $("<span>").addClass("title-server").html(title);

    $(ancore).append(icon).append(titleServer);
    $(boxShoableGozaresh).append(ancore).append(checkbox);

    return $(itemGozareshServer).append(boxShoableGozaresh).append(checkbox)
        .on("click", function (e) {
            const selectBtm = document.querySelector(".gozarsh-btn-container.d-none [type=SELECT]");
            if (!enableClickItemServices || !selectBtm) {
                return;
            }

            $(this).toggleClass("checked").find(".icon-checkbox").toggleClass("checked");
    });
}

$(".gozarsh-btn-container").on("click", ".btn", function (e) {
    if ($(this).hasClass("is-fetched") || $(this).hasClass("disabled")) {
        return;
    }

    const type = $(this).attr("type");
    const func = functionalBtnGozarsesh[type];

    if (!func) return;

    func(this);

});
const functionalBtnGozarsesh = {
    "SELECT": (node) => {
        console.log("SELECT");
        setDNone(node);
        visibleNodeCheckBoxGozaresh();
    },
    "CANCEL": (node) => {
        console.log("CANCEL");
        setDNone(node);
        hiddenNodeCheckBoxGozaresh();
        clearCheckedNodeCheckBoxGozaresh();
    },
    "SUBMIT": (node) => {
        console.log("SUBMIT");
        checkedNodes = document.querySelectorAll(".item-gozaresh-server .icon-checkbox.checked");
        if (!checkedNodes || checkedNodes.length == 0) {
            alert("هیج ایتمی را انتخاب نکرده اید");
            return;
        }
        checkBoxNodes = document.querySelectorAll(".item-gozaresh-server .icon-checkbox");
        if (checkedNodes.length == checkBoxNodes.length) {
            console.log("All Of Services");
            getAllServicesCondition(node);
            return;
        }
        const servicesIds = [...checkedNodes].map((checkbox) => $(checkbox).parent().attr("servicesid"));
        callServerForConditionServices(servicesIds);
        fetchedbtnCalss(node);
    },
    "SUBMIT_ALL": function (node) {
        console.log("All Of Services");
        getAllServicesCondition(node);
    }
};
function getAllServicesCondition(node) {
    $(".item-gozaresh-server").addClass("checked");
    callServerForConditionServices();
    fetchedbtnCalss(node);
}
function setDNone(node) {
    $(".gozarsh-btn-container").removeClass("d-none");
    $(node).parent().addClass("d-none");
}
function visibleNodeCheckBoxGozaresh() {
    $(".item-gozaresh-server")
        .addClass("is-checking")
        .find(".icon-checkbox")
        .removeClass("hidden");
}
function hiddenNodeCheckBoxGozaresh() {
    $(".item-gozaresh-server")
        .removeClass("is-checking")
        .find(".icon-checkbox")
        .addClass("hidden");
}
function clearCheckedNodeCheckBoxGozaresh() {
    return $(".item-gozaresh-server .icon-checkbox.checked").removeClass("checked");
}
function fetchedbtnCalss(node) {
    $(node).removeClass("disabled").addClass("is-fetched").find(".spinner").addClass("isLoad");
}
function isLoadingFetchDataServices(node) {
    enableClickItemServices = false;
    clearCheckedNodeCheckBoxGozaresh();
    hiddenNodeCheckBoxGozaresh();
    $(".gozarsh-btn-container .btn").addClass("disabled");
    $(".item-gozaresh-server.checked")
        .removeClass("checked")
        .addClass("show-off");

}
function endLoadingFetchDataServices() {
    $(".gozarsh-btn-container .btn.disabled")
        .removeClass("disabled");

    $(".gozarsh-btn-container .btn.is-fetched")
        .removeClass("is-fetched")
        .find(".spinner")
        .removeClass("isLoad");

    $(".item-gozaresh-server.show-off")
        .removeClass("show-off");
    visibleNodeCheckBoxGozaresh();
}
function callServerForConditionServices(data = {}) {
    const jpars = JSON.stringify({ servicesIds: data });
    isLoadingFetchDataServices();
    $.ajax({
        url: __API_SERVICES_HANDLER__,
        data: jpars,
        contentType: "application/json",
        type: data instanceof Array ? "POST" : "GET",
        success: function (res) {
            endLoadingFetchDataServices();
            alert("با موفقیت انجام شد");
        },
        error: function (e) {
            endLoadingFetchDataServices();
            alert("خطا هنگام اجرای عملیات");
        }
    });

}
