const headerMenuDummy = [
    {
        title: "صفحه اصلی",
        href: "/nit",
        options: [
            {  },
        ],
    },
    {
        title: "خدمات مرکز",
        href: "/index",
        options: [
            { title: "پست الکترونیک", href: "/index" },
            { title: "شبکه ی مجازی دانشگاه", href: "/index" },
            { title: "FTP (تخصصي دانشكده ها - عمومي)", href: "/index" },
            { title: "مركز محاسبات سريع(HPC)", href: "/index" },
            { title: "فايل هاي مورد نياز", href: "/index" },
            { title: "FTP (مستندات دروس- DOC)", href: "/index" },
        ],
    },
    {
        title: "درباره ی مرکز",
        href: "/index",
        options: [
            { title: "معرفی مرکز", href: "/index" },
            { title: "کارکنان مرکز", href: "/index" },
        ],
    },
    {
        title: "درخواست ها",
        href: "/index",
        options: [
            { title: "درخواست ایمیل", href: "/index" },
            { title: "درخواست VPN", href: "/index" },
            { title: "درخواست DOCS", href: "/index" },
            { title: "درخواست خدمات HPC", href: "/index" },
        ],
    },
    {
        title: "فرم ها و مقررات",
        href: "/index",
        options: [
            { title: "قوانین و مقررات", href: "/index" },
            { title: "فرم های مورد نیاز", href: "/index" },
            { title: "قوانین و مقررات سایت ارشد", href: "/index" },
            { title: "قوانین و مقررات کارشناسی", href: "/index" },

        ],
    },
  {
    title: "تماس با ما",
    href: "/nit/page/contactus",
    options: [
    ],
  },
  {
    title: "درخواست همکاری",
    href: "/nit/career/requestform",
    options: [
    ],
  },
];
const getAncore = (title, href) => $("<a>").html(title).attr({ href });

const MenuDummyData = $(".Menu-Dummy-Data");
createMenuDummyData(MenuDummyData);
function createMenuDummyData(parent) {
    if (parent == null)
        return;


    const menuBar = $("<ul>").addClass("menu-bar");

    const menuBarItemsNode = headerMenuDummy.map((item, index) => createMenuBarItem(item, index));
    console.log(menuBarItemsNode);
    $(menuBar).append(menuBarItemsNode);
    $(parent).empty().append(menuBar);
}

function createMenuBarItem(item, index) {
    const menuBarItem = $("<li>")
        .addClass("menu-bar-item")
        .attr({ key: index });
    const aTitle = getAncore(item.title, item.href);
    const title = $("<div>")
        .addClass("title")
        .append(aTitle);

    const menuBarInner = createMenuBarInner(item.options);
    if(menuBarInner !=null){
      console.log(menuBarInner);

      $(menuBarItem).addClass("is-content");
    }
    return $(menuBarItem)
        .append(title)
        .append(menuBarInner);
}
function createMenuBarInner(options = []) {
  if(!options || options.length == 0 )
    return  null;

    const box = $("<ul>").addClass("menu-bar-inner");
    innerItemMenuNodes = options.map(
        ({ title, href }) => {
            const innerItemMenu = $("<li>").addClass("inner-item-menu");
            const ancore = getAncore(title, href);
            return $(innerItemMenu).append(ancore);
        }
    );
    return $(box).append(innerItemMenuNodes);
}
