class News {
    constructor(id, title = "", pic = "", minDesc = "", likeCount = 0, commentCount = 0, isLike = false, desc = "", comments = [], sender) {
        this.id = id;
        this.title = title;
        this.minDesc = minDesc;
        this.desc = desc;
        this.pic = pic;
        this.comments = comments;
        this.likeCount = likeCount;
        this.commentCount = commentCount;
        this.sender = sender;
        this.isLike = isLike;
        this.endCganeLike = isLike;
        this.btnTypes = [
            { type: "LIKE", class: "icon-like" },
            { type: "COMMENT", class: "icon-comment" },
            { type: "SHARE", class: "icon-share" }
        ];
        this.like = null;
        this.comment = null;
        this.share = null;
        this.current = null;
        this.btnBoxCurrent = null;
        this.commentBox = null;
        this.commentBoxInput = null;
        this.textBox = null;
        this.sendComment = null;
        this.likeBoxNode = null;
        this.replyId = null;
    }
    get iconLike() {
        return $(this.like).find(".icon");
    }
    setEndCganeLike() {
        this.endCganeLike = this.isLike;
    }
    setIsLike(isLike) {
        this.isLike = isLike;
        if (this.isLike)
            this.likeNews();
        else
            this.disLike();
    }
    likeNews = () => {
        this.isLike = true;
        this.likeCount++;
        $(this.iconLike).addClass("liked");
        this.setCountLikeNode();
    };
    disLike = () => {
        this.isLike = false;
        this.likeCount--;
        $(this.iconLike).removeClass("liked");
        this.setCountLikeNode();
    }
    toggleLike = () => {
        this.setIsLike(!this.isLike);
    }
    setCountLikeNode() {
        $(this.likeBoxNode).find(".counter").html(this.likeCount);
    }
    clickHandler(func, clickableItem = true) {
        $(this.like).on("click", (e) => func(e, this, this.like));
        $(this.comment).on("click", (e) => func(e, this, this.comment));
        $(this.share).on("click", (e) => func(e, this, this.share));

        $(this.sendComment).on("click", (e) => func(e, this, this.sendComment));
        if (!clickableItem) {
            return;
        }
        $(this.current).on("click", (e) => {
            $(e.target).addClass("__CLICKED_ITEM__");
            const items = $(".more-box").find(".__CLICKED_ITEM__");
            $(e.target).removeClass("__CLICKED_ITEM__");

            if (items.length) {
                console.log("no");
                return;
            }
            func(e, this, this.current);
        });
    }
    renderMin() {
        this.current = $("<li>").addClass("col").attr({ key: this.id });
        const itemNews = $("<div>").addClass("item-news").attr({ key: this.id });
        const picHolder = $("<div>").addClass("pic-holder");
        const pic = $("<img>").addClass("pic-news").attr({ src: this.pic });
        const ditaleBox = $("<div>").addClass("ditale-box");
        const title = $("<h4>").addClass("title-news").html(this.title);
        const miniDescribe = $("<p>").addClass("mini-describe").html("خلاصه خبر :  " + this.minDesc);
        const seeMore = $("<a>")
            .addClass("see-more-ditale")
            .html("مشاهده کامل خبر")
            .attr({ href: "single.htm?id=" + this.id });
        this.addButtonFunctional();

        $(picHolder).append(pic);
        $(ditaleBox).append(title).append(miniDescribe).append(this.btnBoxCurrent).append(seeMore);
        $(itemNews).append(picHolder).append(ditaleBox);
        $(this.current).append(itemNews);
    }
    render() {
        this.current = $("<div>").addClass("item-news").attr({ key: this.id });
        const picHolder = $("<div>").addClass("pic-holder");
        const pic = $("<img>").addClass("pic-news").attr({ src: this.pic });
        const ditaleBox = $("<div>").addClass("ditale-box");
        const title = $("<h4>").addClass("title-news").html(this.title);
        const miniDescribe = $("<p>").addClass("mini-describe").html("خلاصه خبر :  " + this.minDesc);
        const describe = $("<p>").addClass("describe");
        if (this.desc.length > 200) {
            $(describe).addClass("min").html(this.desc.substr(0, 120));
            const more = $("<span>").addClass("see-more").html("مشاهده بیشتر");
            $(more).on("click", (e) => $(describe).removeClass("min").html(this.desc));

            $(describe).append(more);
        }
        else {
            $(describe).html(this.desc);
        }
        console.log(this.sender);
        const senderBox = new Sender(this.sender);
        senderBox.render();
        console.log(senderBox.current);
        this.addButtonFunctional();
        $(picHolder)
            .append(pic);
        $(ditaleBox)
            .append(title)
            .append(miniDescribe)
            .append(describe)
            .append(this.btnBoxCurrent);
        $(this.current)
            .append(picHolder)
            .append(senderBox.current)
            .append(ditaleBox)

        this.addInputComment(true);
        //this.addCommentBox(true);
    }
    addButtonFunctional(iscurrent) {
        const moreBox = $("<div>").addClass("more-box-holder");
        const boxTextDitale = $("<div>").addClass("ditale-more-box");
        this.likeBoxNode =
            getCounterNode("پسندیده", this.likeCount, "like-counter-box", "like-text", "like-counter");
        const splitNode = $("<span>").addClass("split").html(",");
        const commentBoxNode =
            getCounterNode("نظرات", this.commentCount, "comment-counter-box", "comment-text", "comment-counter");

        const bottomBox = $("<ul>").addClass("more-box");
        const btnsNode = this.btnTypes.map(
            dit => {
                const pIc = $("<li>").addClass("more-box-item").attr({ type: dit.type });
                const ic = $("<li>").addClass("icon").addClass(dit.class).html(dit.type[0]);
                this[dit.type.toLocaleLowerCase()] = pIc;
                return $(pIc).append(ic);
            }
        );

        if (this.isLike) {
            $(this.iconLike).addClass("liked");
        }
        $(boxTextDitale).append(this.likeBoxNode).append(splitNode).append(commentBoxNode);
        $(bottomBox).append(btnsNode);

        $(moreBox).append(boxTextDitale).append(bottomBox);

        this.btnBoxCurrent = moreBox;

        if (iscurrent)
            this.addTocurrent(this.commentBox);

    }
    addInputComment(iscurrent) {

        this.commentBoxInput = $("<div>").addClass("comment-text-box");
        this.textBox = $("<textarea>").addClass("comment-text")
            .attr({ row: 3, placeholder: "نظر خود را وارد کنید" });
        this.sendComment = $("<span>")
            .addClass("comment-send")
            .addClass("icon")
            .addClass("icon-send")
            .attr({ type: "SEND_COMMENT" });
        // .html("ارسال")

        $(this.commentBoxInput).append(this.textBox).append(this.sendComment);

        if (iscurrent)
            this.addTocurrent(this.commentBoxInput);

    }
    addCommentBox(iscurrent) {
        const commentBoxContainer = $("<div>").addClass("comment-box-container");

        const title = $("<h5>").addClass("title-comment-box").html("نظرات");

        this.commentBox = $("<div>").addClass("comment-box");

        $(commentBoxContainer).append(title).append(this.commentBox);

        if (iscurrent)
            this.addTocurrent(commentBoxContainer);

    }
    addTocurrent(node) {
        $(this.current).append(node);
    }
    //comment
    loadingSendComment() {
        $(this.sendComment).removeClass("icon-send").addClass("spinner").addClass("isLoad");
        console.log(this.sendComment);

    }
    stopSendComment() {
        $(this.sendComment).removeClass("spinner").removeClass("isLoad").addClass("icon-send");
        console.log(this.sendComment);
        news.replyId = null;
    }
}
function getCounterNode(text, count, classBox, classText, classCount) {
    const boxNode = $("<span>").addClass("counter-box").addClass(classBox);
    const textNode = $("<span>").addClass("text").addClass(classText).html(text);
    const counterNode = $("<span>").addClass("counter").addClass(classCount).html(count);

    return $(boxNode).append(textNode).append(counterNode);
}