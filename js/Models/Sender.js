class Sender {
    constructor({id, username, pic}) {
        this.id = id;
        this.username = username;
        this.pic = pic;
        this.current=null;
        this.senderNode=null;
        this.senrderName=null;
        this.senrderPic=null;
    }
    setClickItemHandler(func) {
        $(this.senderNode).on(this.senderNode, (e) => func(this));
    }
    render() {
        this.senderNode = $("<span>").addClass("sender-box-container").attr({ id: this.id });
        this.senrderName = $("<span>").addClass("sender-name").html(this.username);
        this.senrderPic = $("<div>").addClass("sender-pic");
        if (!this.pic)
            $(this.senrderPic).addClass("no-pic");
        else {
            const img = $("<img>").attr({ src: this.sender.pic });
            $(this.senrderPic).append(img);
        }
        $(this.senderNode).append(this.senrderPic).append(this.senrderName);

        this.current=this.senderNode;
    }
}