class Modal {
    constructor(loadMessage,errorMessage){
        this.isShow = false;
        this.isLoading = false;
        this.backDrop=null;
        this.modal=null;
        this.modalContent=null;
        this.loadingBox=null;
        this.spinner=null;
        this.textSpinner=null;
        this.loadMessage = loadMessage;
        this.errorMessage = errorMessage;
    }
    Create(){
        this.modal = $("<div>").addClass("modal").addClass("close");
        this.backDrop = $("<div>").addClass("back-drop");
        this.modalContent = $("<div>").addClass("content-modal");
        this.loadingBox = $("<div>").addClass("box-loading").addClass("stop");
        this.spinner  = $("<div>").addClass("spinner").addClass("isLoad");
        this.textSpinner = $("<div>").html(this.loadMessage);

        $(this.loadingBox).append(this.spinner).append(this.textSpinner);
        $(this.backDrop).append(this.loadingBox);
        $(this.modal).append(this.backDrop).append(this.modalContent);
        $(document.body).append(this.modal);

        $(this.backDrop).on("click",(e)=>{
            this.EndLoad();
            this.Close();
        });
    }
    Load(){
        $(this.spinner).addClass("isLoad");
        this.setAddRemoveClass(this.loadingBox,"start","stop");
    }
    EndLoad(){
        this.setAddRemoveClass(this.loadingBox,"stop","start");
    }
    Error(){
        $(this.spinner).removeClass("isLoad");
        $(this.textSpinner).html(this.errorMessage);   
    }
    Open(){
        this.isShow=true;
        this.setAddRemoveClass(this.modal,"open","close");
    }
    Close(){
        this.isShow=false;
        this.setAddRemoveClass(this.modal,"close","open");
        $(this.modalContent).removeClass("is-content")
    }
    setAddRemoveClass(node,add,remove){
        $(node).addClass(add).removeClass(remove);
    }
    setContent(children){
        $(this.modalContent)
        .addClass("is-content")
        .empty()
        .append(children);
    }
}