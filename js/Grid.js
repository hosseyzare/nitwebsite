let SelectNumbers;
let SelectSomthings;
let SearchBoxNode;
let GridBody;
let GridPagination;
let isLoadingCallServer = false;
let editPagePoint = "edit.htm";
let API_GRID = "";
let DELETE_API_GRID = "";
let createrFunctionDom = () => {};
//API => /{api}/{controller}
//DELETE => method=DELETE => /{api}/{controller}/{id}
function CreateGrid({
  gridContainer = null,
  gridThem = "default",
  gridTitle = "",
  optionsViewItemNumber = [],
  optionsViewItemWidthSomthing = [],
  headerTitles = [],
  API = "",
  pageEdit,
  deleteAPI = "",
  domCreaterFunction = null,
}) {
  if (gridContainer == null) return;
  DELETE_API_GRID = deleteAPI;
  API_GRID = API;
  if (pageEdit) {
    editPagePoint = pageEdit;
  }
  const grid = $("<div>")
    .addClass("grid")
    .addClass("grid-them")
    .addClass(gridThem);
  const gridTitleNode = $("<h1>").addClass("grid-title").html(gridTitle);
  const gridContainerNode = $("<div>").addClass("grid-container");

  const gridTop = createGridTop(
    optionsViewItemNumber,
    optionsViewItemWidthSomthing
  );
  if (domCreaterFunction == null) {
    createrFunctionDom = createGridBodyItems;
  } else {
    createrFunctionDom = domCreaterFunction;
  }
  let GridHeader = headerTitles ? createHeaderGrid(headerTitles) : null;
  GridBodyBox = $("<div>").addClass("grid-body");
  GridBody = $("<ul>").addClass("grid-content");

  $(GridBodyBox).append(GridHeader).append(GridBody);
  const gridPaginationBox = $("<div>").addClass("grid-pagination");
  GridPagination = $("<ul>").addClass("pagination");

  $(gridPaginationBox).append(GridPagination);
  $(gridContainerNode)
    .append(gridTop)
    .append(GridBodyBox)
    .append(gridPaginationBox);
  $(grid).append(gridTitleNode).append(gridContainerNode);
  $(gridContainer).append(grid);

  API_GRID = API;

  sendCallToGetData();
}
function createLineGrid() {}
function createGridTop(numbers, widthSomthing) {
  const topBoxGrid = $("<div>").addClass("grid-top");

  const flexRow = $("<div>").addClass("flex-row");
  SelectNumbers = createSelectOptionGroup("تعداد : ", numbers, "number-select");
  SelectSomthings = createSelectOptionGroup(
    "بر اساس : ",
    widthSomthing,
    "somthing-select"
  );
  $(flexRow).append(SelectNumbers).append(SelectSomthings);
  const searchBox = $("<div>")
    .addClass("group-item")
    .addClass("search-box")
    .addClass("shadow-box-main");
  SearchBoxNode = $("<input>").attr({
    type: "text",
    id: "search-grid",
    placeholder: "سرچ کنید",
  });
  $(searchBox).append(SearchBoxNode);

  $(topBoxGrid).append(searchBox).append(flexRow);

  return topBoxGrid;
}
function createSelectOptionGroup(title, options, idSelect) {
  const groupItem = $("<div>").addClass("grid-item");
  const label = $("<label>").html(title);
  const select = $("<select>")
    .attr({ id: idSelect })
    .addClass("shadow-box-main");
  $(select).on("change", sendCallToGetData);
  [...options].forEach((op) => {
    const option = $("<option>").val(op.value).html(op.title);
    $(select).append(option);
  });
  $(groupItem).append(label).append(select);
  return groupItem;
}
function createHeaderGrid(headerOptionsArr = []) {
  const headerGrid = $("<ul>").addClass("grid-header");

  if (!headerOptionsArr || headerOptionsArr.length == 0) return headerGrid;

  headerOptionsArr.map((option) => {
    const title = $("<li>").addClass("grid-header-item").html(option);
    $(headerGrid).append(title);
  });

  return headerGrid;
}
let dataDummy = {
  gridItems: [
    // { id: 1, pic: "sajjad.png", options: { title: "عنوان خبر", startDate: "1399/55/55", endDate: "1399/55/55", showable: "بله", } }
    // , { id: 3, pic: "sajjad.png", options: { key:value } }
  ],
  ShowNumberitems: 5,
  indexPage: 1,
  maxPage: 20,
};
const timer = 0; //2000;
function sendCallToGetData() {
  if (isLoadingCallServer == true) return;

  const indexPage = $(GridPagination).find(".selected").attr("key");
  const itemCount = +$(SelectNumbers).find("option:selected").text();

  isLoadingGrid();
  //1, 5, API
  var formData = { indexPage: indexPage ? +indexPage : 1, itemCount };
  $.ajax({
    url: API_GRID,
    type: "POST",
    data: formData,
    encode: true,
    dataType: "json",
    success: (res) => {
      endLoadingGrid();

      console.log(res);

      createrFunctionDom(GridBody, res.gridItems);
      createPagination(res.indexPage, res.maxPage);
    },
    error: (e) => {
      document.body.innerHTML += e.responseText;
    },
  });
}
function endLoadingGrid() {
  $(GridBody).empty();
  isLoadingCallServer = false;
}
function isLoadingGrid() {
  isLoadingCallServer = true;
  $(GridBody).empty();
  $(GridPagination).empty();
  const boxSpinner = $("<li>")
    .addClass("row")
    .addClass("center-center-container");
  const spinner = $("<span>").addClass("spinner").addClass("isLoad");
  $(boxSpinner).append(spinner);
  $(GridBody).append(boxSpinner);
}
function editItemGrid(e) {
  //way two
  const idKe = $(this).attr("key");
  const href = window.location.href.split("/");
  href.pop();
  href.push("edit.htm?id=" + idKe);
  window.location.href = href.join("/");
}
function deleteItemGrid(e) {
  if ($(this).hasClass("sended")) {
    console.log("sended");
    return;
  }
  const btn = this;
  const idKe = $(btn).addClass("sended").attr("key");
  startLoadingForDelete(btn);
  $.ajax({
    url: DELETE_API_GRID+"/"+idKe,
    contentType: "application/json",
    type: "post",
    success: function (res) {
      // alert("با موفقیت حذف شد");
      console.log("با موفقیت حذف شد");
      endLoadingForDelete(btn);
      sendCallToGetData();
    },
    error: function (e) {
      // alert("خطا هنگام حذف");
      console.log("خطا هنگام حذف");
      endLoadingForDelete(btn);
      $(btn).removeClass("sended");
    },
  });
  // alert("delete item for id : " + idKe);
}
function startLoadingForDelete(btn) {
  $(btn).parent().parent().addClass("check-remove");
  $(btn).removeClass("btn-Delete").addClass("spinner").addClass("isLoad").css({
    opacity: 1,
    width: 20,
    height: 20,
    margin: 0,
    borderWidth: 3,
    borderColor: "rgba(222, 50, 50, 0.2)",
    borderTopColor: "rgba(222, 50, 50, 1)",
  });
}
function endLoadingForDelete(btn) {
  $(btn).parent().parent().removeClass("check-remove");
  $(btn)
    .removeAttr("style")
    .removeClass("spinner")
    .removeClass("isLoad")
    .addClass("btn-Delete");
}
function createGridBodyItems(body, gridItems) {
  gridItems.forEach((dD, i) => {
    const li = $("<li>").addClass("row").attr({ key: dD.id });
    const ul = $("<ul>").addClass("col-box-grid");
    if (dD.pic) {
      const imgItem = $("<li>")
        .addClass("grid-body-item")
        .append($("<img>").attr({ src: dD.pic }));
      $(ul).append(imgItem);
    }
    Object.keys(dD.options).forEach((opKy) => {
      const gridBodyItem = $("<li>")
        .addClass("grid-body-item")
        .attr({ key: opKy })
        .html(dD.options[opKy]);
      $(ul).append(gridBodyItem);
    });
    const btnEdit = $("<a>")
      .addClass("btn")
      .addClass("btn-Edit")
      .attr({ href: "/nit" + editPagePoint + "/" + dD.id });
    //.on("click",editItemGrid);
    const btnDelete = $("<span>")
      .addClass("btn")
      .addClass("btn-Delete")
      .attr({ key: dD.id })
      .on("click", deleteItemGrid);
    const btnHolder = $("<li>")
      .addClass("grid-body-item")
      .addClass("btn-holder")
      .append(btnEdit)
      .append(btnDelete);
    $(ul).append(btnHolder);
    $(li).append(ul);
    appendTimer(li, i * 400);

    function appendTimer(node, timer) {
      setTimeout(() => $(body).append(node), timer);
    }
  });
}
function createPagination(index, max) {
  $(GridPagination).empty();
  let start = 1,
    end = max;
  let left = null,
    right = null;
  if (max > 5) {
    start = index - 2;
    end = index + 2;
    if (end > max) {
      start -= 2;
      end = max;
    } else
      right = $("<li>").addClass("item-pagination").addClass("right").html(">");
    if (start < 1) {
      start = 1;
      end += 2;
    } else
      left = $("<li>").addClass("item-pagination").addClass("left").html("<");

    if (right != null) $(right).attr({ key: end + 1 });
    if (left != null) $(left).attr({ key: start - 1 });
  }
  const pageItems = [];
  for (; start <= end; start++) {
    const li = $("<li>")
      .addClass("item-pagination")
      .html(start)
      .attr({ key: start });
    if (start == index) {
      $(li).addClass("selected");
    }
    pageItems.push(li);
  }

  $(GridPagination)
    .append(left)
    .append(pageItems)
    .append(right)
    .on("click", ".item-pagination", function (e) {
      $(this).parent().find(".item-pagination").removeClass("selected");
      $(this).addClass("selected");

      sendCallToGetData();
    });
}
