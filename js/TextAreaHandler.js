//17px => one letter is 4px
let config = {
    textarea: null,
    lineHeight: 20,
    minLine: 1,
    maxLine: -1,
    lastCountLin: 1,
}
function textAreaHandler(textarea, lineHeight, minLine, maxLine) {
    config = {
        textarea,
        lineHeight: lineHeight ? lineHeight : config.lineHeight,
        minLine: minLine ? minLine : config.minLine,
        maxLine: maxLine ? maxLine : config.maxLine,
    }
    console.log("start");
    $(textarea).css({
        height: minLine * lineHeight,
    });
    $(textarea).on("keyup", setSizeTextAreaInOnChange);
    $(textarea).on("click", setSizeTextAreaInOnChange);
}

var lineNumber = (tempString) => tempString.split('\n').length;
var lineCount = (str) => str.match(/[\s\S]*(?=\[source\])/)[0].replace(/[^\n]/g, "")?.length;

function setSizeTextAreaInOnChange() {
    const str = $(this).val();
    
    let count = lineNumber(str);

    if (config.lastCountLin == count)
        return;

    if (count < config.minLine)
        count = config.minLine;
    else if (count > config.maxLine)
        count = config.maxLine;

    $(this).css({ height: count * config.lineHeight, });
}