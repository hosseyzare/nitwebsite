notifitionBoxCreate();
const notifBox = document.querySelector(".notif-Box .notif");
const btnSendForm = document.querySelectorAll(".btn-send-form");
const imgHodlerSelected = document.querySelector(".img-hodler-selected");
let isFetching = false;
if (imgHodlerSelected) {
    const fileUploadPic = document.querySelector(".file-upload-pic");
    console
    if (fileUploadPic)
        fileUploadPic.addEventListener('change', function (e) {
            let file = this.files[0];
            const img = $("<img>").attr({
                src: "",
                width: "100%",
                height: "auto"
            });
            $(imgHodlerSelected)
                .empty()
                .append(img);
        });
}
function notifitionBoxCreate() {
    const notifBox = $("<div>").addClass("notif-Box");
    const notif = $("<div>").addClass("notif");
    const describeBox = $("<div>").addClass("describe-box");
    const textNotifOk = $("<p>").addClass("text-notif").addClass("ok").html("با موفقیت ثبت شد");
    const textNotifNotOk = $("<p>").addClass("text-notif").addClass("not-ok").html("خطا هنگام اجرای عملیات");
    $(describeBox)
        .append(textNotifOk)
        .append(textNotifNotOk);
    $(notif)
        .append(describeBox);
    $(notifBox)
        .append(notif);
    $(document.body).prepend(notifBox);
}
function callServer(ajaxProps, indexLoadBtn) {
    if (isFetching) {
        return;
    }
    const { url, type, data, success = (res) => { }, error = (e) => { } } = ajaxProps;
    lodingSend(indexLoadBtn);
    $.ajax({
        url: url,
        data:data,
        dataType: 'json',
        encode: true,
        type: type,
        success: function (res) {
            console.log("Ok");
            OpenNotif(["ok"]);
            CloseNotif(3);
            endLoadingSend(indexLoadBtn);
            success(res);


        },
        error: function (e) {
            OpenNotif(["not-ok"]);
            CloseNotif(3);
            endLoadingSend(indexLoadBtn);
            error(e);
        }
    });
}
function OpenNotif(customclass) {
    $(notifBox)
        .addClass("open");
    if (customclass instanceof Array)
        customclass.forEach(clas => $(notifBox).addClass(clas));
}
function CloseNotif(timeout) {
    setTimeout(() => {
        $(notifBox)
            .removeClass("open")
            .removeClass("ok")
            .removeClass("not-ok");
    }, timeout * 1000);

}
function lodingSend(index) {
    isFetching=true;
    console.log($(btnSendForm).eq(index), index);
    $(btnSendForm)
        .eq(index)
        .addClass("clicked")
        .find(".spinner")
        .addClass("isLoad");
}
function endLoadingSend(index) {
    $(btnSendForm).eq(index)
        .removeClass("clicked")
        .find(".spinner")
        .removeClass("isLoad");

    isFetching = false;
}