let getPosLeft = (i, sizeItem) => (i) * sizeItem + (i * 2 + 1) * 30 + 20;
function createMiniSliderOwner(config) {
    const sizeScreen = $(window).width();
    const responsiveSize = Object.keys(config.responsive).reverse().find(c => c <= sizeScreen);
    if (responsiveSize < 0)
        return;
    const countItems = config.responsive[responsiveSize];

    const sliderOwner = $(".slider-owner");
    $(sliderOwner).map((index, slide) => {
        const countGridItems = $(slide).find(".grid-item").length;
        const boxBtns = btnCreateWidthBox(1, countGridItems, slide);
        const left = btnSlider("left-btn", "", slide).attr({ dir: -1 });
        const right = btnSlider("right-btn", "", slide).attr({ dir: 1 });
        const sizeBox = $(slide).find(".grid").width();
        const hBox = $(slide).find(".grid").height();
        const marginSide = 30;
        const sizeItem = (sizeBox - marginSide * countItems) / countItems - 20;
        $(slide)
            .find(".grid").css({
                height: hBox,
                boxSizing: "none",
            })
            .find(".grid-item")
            .css({
                minWidth: sizeItem,
                width: sizeItem,
                maxWidth: sizeItem,
                position: "absolute",
            }).map((i, el) => $(el).attr({ id: i }));
        if (countGridItems > countItems) {
            $(slide)
                .append(boxBtns)
                .append(left)
                .append(right)
        }
        setPositionSlider(slide, false);
    });
    $(sliderOwner)
        .on("mousedown", ".grid", function (e) { selectSliderBox(e, this) })
        .on("click", ".btns-box li", function (e) {
            if ($(this).hasClass("active")) {
                return;
            }

            let prevIndex = $(this).parent().find('.active').index();
            let nextIndex = $(this).index();

            size = nextIndex - prevIndex;
            const slide = $(this).parent().parent().parent();
            console.log(slide);
            moveMultyItemOfSlider(slide, size);
        })
        .on("click", ".btn-slider", function (e) {

            const dir = $(this).attr("dir") || 0;

            const slide = $(this).parent();

            console.log({ dir, slide })

            moveIndexOfSldier(slide, dir);
            console.log("sallam");
        });
}
function moveMultyItemOfSlider(slider, sizeMove) {
    let dir = sizeMove > 0 ? 1 : -1;
    sizeMove = Math.abs(sizeMove);
    while (sizeMove-- > 0) {
        setTimeout(() => {
            moveIndexOfSldier(slider, dir);
        }, sizeMove * 300);
    }
}
function moveIndexOfSldier(slide, dir) {
    let indexActive = +$(slide).find(".btns-box .active").removeClass("active").index() + (+dir);
    const sizeAllBtns = $(slide).find(".grid-item").length;
    if (indexActive >= sizeAllBtns) {
        indexActive = 0;
    }
    else if (indexActive < 0) {
        indexActive = sizeAllBtns - 1;
    }
    console.log({ indexActive })
    $(slide)
        .find(".btns-box li")
        .eq(indexActive)
        .addClass("active");

    let W = $(slide).find(".grid-item:first-child").width();
    if (dir == 1) {
        const size = $(slide).find(".grid-item").length;
        const first = $(slide)
            .find(".grid-item:first-child")
            .css({ left: getPosLeft(size, W) });
        $(slide).find(".grid").append(first);
    }
    else if (dir == -1) {
        const last = $(slide)
            .find(".grid-item:last-child")
            .css({ left: getPosLeft(-1, W) });
        $(slide).find(".grid").prepend(last);
    }
    // setTimeout(() => { $(slide).find(".hidden").removeClass("hidden")}, 500);
    setPositionSlider(slide);
}
function setPositionSlider(slide, isOverflow = true) {
    sizeItem = $(slide).find(".grid-item").eq(0).outerWidth();

    $(slide).find(".grid-item").map((i, gi) => {
        let x = isOverflow ? i : i + 1 - 1;
        $(gi).css({
            left: getPosLeft(x, sizeItem)
            //(i - 1) * sizeItem + (i * 2) * 30 - 7
        });
    });
}
function btnCreateWidthBox(indexActive, count, slide) {
    const boxBtnContainer = $("<div>").addClass("box-btn-container");

    const btnsBox = $("<ul>").addClass("btns-box");

    const nodesBtn = [...Array(count)].map(c => $("<li>").addClass("btn-item-box-slide"));

    $(nodesBtn[indexActive - 1]).addClass("active");

    btnsBox.append(nodesBtn);
    boxBtnContainer.append(btnsBox);

    return boxBtnContainer;
}
function btnSlider(clas, text) {
    return $("<span>").html(text)
        .addClass("btn-slider")
        .addClass(clas);
}
let currentSizeScren = $(window).width();
//drag and Drop
function selectSliderBox(e, grid) {
    grid.addEventListener("mousemove", moveGrid);
    grid.addEventListener("mouseup", mouseUpGrid);
    console.log("Dwon");
    const startX = e.clientX;
    $(grid).parent().addClass("user-select-none");
    function moveGrid(e) {
        const moveX = e.clientX;
        const moveSize = moveX - startX;
        setPositionSliderWidthMoveVar($(grid).parent(), moveSize);
        console.log("Move", moveSize);
    }

    function mouseUpGrid(e) {
        console.log("Up");
        $(grid).parent().removeClass("user-select-none");
        grid.removeEventListener("mousemove", moveGrid);
        grid.removeEventListener("mousemove", moveGrid);
    }
}
function setPositionSliderWidthMoveVar(slide, move) {
    let W = $(slide).find(".grid-item:first-child").width();
    $(slide).find(".grid-item").map((i, gi) => {

        let left = $(gi).position().left + move;
        // console.log({left: $(gi).position().left + move});
        $(gi).css({ left });
    });
    if (move < 0) {
        const first = $(slide).find(".grid-item:first-child");
        let posL = getPosLeft($(first).index(), W);
        let posLM = $(first).position().left;
        console.log(posLM, " < ", posL - W)
        if (posLM < posL - W) moveIndexOfSldier(slide, 1);

    } else {
        const last = $(slide).find(".grid-item:last-child");
        let posL = getPosLeft($(last).index(), W);
        let posLM = $(last).position().left;
        console.log(posL + W, " < ", posLM)
        if (posL + W < posLM) moveIndexOfSldier(slide, -1);
    }

}
