<?
if (!defined('test')) { echo "Forbidden Request"; exit; }

global $config;
$config['db']['host'] = 'localhost';
$config['db']['user'] = 'root';
$config['db']['pass'] = '';
$config['db']['name'] = 'nitwebsite';

$config['lang'] = 'fa';
//$config['lang'] = 'eng';

$config['salt'] = 'vd8loyqsdsqdnqexq5fhn4quk0j5rs';
$config['base'] = '/nit';