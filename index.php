<?php

define('test', true);
require_once('system/loader.php');


$uri = getRequestUri();
$uri = str_replace(baseUrl() . '/', '/', $uri);

$parts = explode('/', $uri);
$controller = $parts[1];
if(isset($parts[2])){
  $method = $parts[2];
}

if($parts[1] == null){
  $controller = 'mainpage';
  $method = 'loadpage';
}


$params = array();
$imax = count($parts);
for ($i=3; $i<$imax; $i++){
  $params[] = $parts[$i];
}

$controllerClassname = ucfirst($controller) . "Controller";
$controllerInstance = new $controllerClassname();
call_user_func_array(array($controllerInstance, $method), $params);


